export function BaseDAO() {
    this.baseUrl = 'http://localhost:3000'

}
BaseDAO.prototype.prepareRequest = function(method, path, params) {
    let url = new URL(this.baseUrl + path)
    for (let key in params) {
        url.searchParams.set(key, params[key]);
    }
    let xhr = new XMLHttpRequest();
    xhr.open(method, url);
    return xhr;
}