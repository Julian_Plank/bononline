export default function () {
	return `<div><h3>BonOnline</h3>
    <div class="footer-menu">
    <ul class="list-inline">
        <li class="list-inline-item"><a href="/shop/Shop.html">Startseite</a></li>
        <li class="list-inline-item"><a href="/Imprint/Imprint.html">Impressum</a></li>
        <li class="list-inline-item"><a href="/Privacy/Privacy.html">Datenschutz</a></li>
        <li class="list-inline-item"><a href="/Contact/Contact.html">Kontakt</a></li>
    </ul>
    </div><p class="copyright">HTL Pinkafeld | 4AHIF © 2022</p></div>`;
}
