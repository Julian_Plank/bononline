function renderMenu(menu) {
    let newElement = document.createElement("template");
    newElement.innerHTML = `<div class="column">
                <div class="card">
                <img src="../Assets/Images/${menu.type}.jpg" class="card-img-top" alt="User symbol">
                  	<div class="card-body">
                    	<h3 class="card-title">${menu.type[0].toUpperCase()+menu.type.slice(1)}</h3>
                    	<p class="card-text">
                        <h5 class="card-title">${String(Number(menu.price)+Number(menu.tax)).replace(".",",")}0€</h5>
                  	</div>
                </div>
              </div>`;

    return newElement.content;
}

export { renderMenu };