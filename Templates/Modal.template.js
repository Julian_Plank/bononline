export default function() {
    return `<div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="ModalLabel">BonOnline</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      
    </div>
    <div class="modal-footer">
      <button type="button" id="no"class="btn btn-secondary" data-dismiss="modal">Nein</button>
      <button type="button" id="yes" class="btn btn-success">Ja</button>
    </div>
  </div>
</div>
</div>`;
}