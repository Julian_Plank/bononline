function renderProduct(product) {
    let newElement = document.createElement("template");
    let total = String(Number(product.price) + Number(product.tax)).replace(".", ",");
    let indexOfComma = total.indexOf(",");
    indexOfComma == -1 ? total += ",00" : total += "0";
    newElement.innerHTML = `<div class="column" data-id=${product._id}>
                <div class="card">
                <img src="../Assets/Images/${product.name.replace(" ", "_")}.jpg" class="card-img-top" alt="User symbol">
                  	<div class="card-body">
                    	<h3 class="card-title">${product.name}</h3>
                    	<p class="card-text">
                        <h5 class="card-title">${product.type == "product"? total + "€": product.price }</h5>
                  	</div>
                </div>
              </div>`;

    return newElement.content;
}
export { renderProduct };