export default function (total) {
	let total2 = String(total).replace(".", ",");
	let indexOfComma = total2.indexOf(",");
	indexOfComma == -1 ? (total2 += ",00") : (total2 += "0");
	return `<div class="input-group">
    
    <div class="input-group-prepend">
      
      <h2>${String(total2).replace(".", ",")}€</h2>
    </div>
    <button id="oneClick" class="btn btn-success" type="button" >1-Click-Bestellung</button>
  </div>`;
}
