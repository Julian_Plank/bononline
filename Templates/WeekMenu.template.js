export default function(weekMenu) {
    return `<div id="menu">
    <table class="htmlarea-showtableborders" style="margin-left: auto; margin-right: auto">
        <tbody>
            <tr valign="top">
                <td valign="top">&nbsp;</td>
                <td valign="top">
                    <h4>Menü 1 - <span class="price">${weekMenu.standardPrice}</span> </h4>
                </td>
                <td valign="top">
                    <h4>Menü 2 (vegetarisch) - <span class="price">${weekMenu.veggyPrice}</span> </h4>
                </td>
                <td valign="top"></td>
            </tr>
            <tr valign="top" class="menu" data-id="${0}">
                <td>
                    <p>Montag,<br /><span class="date">${weekMenu.days[0].date}</span></p>
                </td>
                <td>
                    Tagessuppe<br /><br /><span class="menuName">${weekMenu.days[0].standardMenu}</span><br /><br />
                    <button class="btn" id="infoBtn">
                            <i class="fa-solid fa-circle-info"></i>
                        </button>
                </td>
                <td>
                    Tagessuppe<br /><br /><span class="menuName">${weekMenu.days[0].veggyMenu}</span><br /><br />
                    <button class="btn" id="infoBtn">
                            <i class="fa-solid fa-circle-info"></i>
                        </button>
                </td>
                <td>
                    <div>
                        <select class="custom-select custom-select-lg md-3" id="category">
                                <option value="1" selected>Menü 1</option>
                                <option value="2">Menü 2</option>
                            </select>
                    </div>
                    <div>
                        <button class="btn btn-success btn-lg btn-block" id="addBtn">
                                <i class="fa-solid fa-cart-plus"></i>
                            </button>
                    </div>
                </td>
            </tr>
            <tr valign="top" class="menu" data-id="${1}">
                <td>
                    <p >Dienstag, <br /><span class="date">${weekMenu.days[1].date}</span></p>
                </td>
                <td>
                    Tagessuppe<br /><br /><span class="menuName">${weekMenu.days[1].standardMenu}</span><br /><br />
                    <button class="btn" id="infoBtn">
                            <i class="fa-solid fa-circle-info"></i>
                        </button>
                </td>
                <td>
                    Tagessuppe<br /><br /><span class="menuName">${weekMenu.days[1].veggyMenu}</span><br /><br />
                    <button class="btn" id="infoBtn">
                            <i class="fa-solid fa-circle-info"></i>
                        </button>
                </td>

                <td>
                    <div>
                        <select class="custom-select custom-select-lg md-3" id="category">
                                <option value="1" selected>Menü 1</option>
                                <option value="2">Menü 2</option>
                            </select>
                    </div>
                    <div>
                        <button class="btn btn-success btn-lg btn-block" id="addBtn">
                                <i class="fa-solid fa-cart-plus"></i>
                            </button>
                    </div>
                </td>
            </tr>
            <tr valign="top" class="menu" data-id="${2}">
                <td>
                    <p >Mittwoch, <br /><span class="date">${weekMenu.days[2].date}</span></p>
                </td>
                <td>
                    Tagessuppe<br /><br /><span class="menuName">${weekMenu.days[2].standardMenu}</span><br /><br />
                    <button class="btn" id="infoBtn">
                            <i class="fa-solid fa-circle-info"></i>
                        </button>
                </td>
                <td>
                    Tagessuppe<br /><br /><span class="menuName">${weekMenu.days[2].veggyMenu}</span><br /><br />
                    <button class="btn" id="infoBtn">
                            <i class="fa-solid fa-circle-info"></i>
                        </button>
                </td>
                <td>
                    <div>
                        <select class="custom-select custom-select-lg md-3" id="category">
                                <option value="1" selected>Menü 1</option>
                                <option value="2">Menü 2</option>
                            </select>
                    </div>
                    <div>
                        <button class="btn btn-success btn-lg btn-block" id="addBtn">
                                <i class="fa-solid fa-cart-plus"></i>
                            </button>
                    </div>
                </td>
            </tr>
            <tr valign="top" class="menu" data-id="${3}">
                <td>
                    <p>Donnerstag, <br /><span class="date">${weekMenu.days[3].date}</span><br /><br /></p>
                </td>
                <td>
                    Tagessuppe<br /><br /><span class="menuName">${weekMenu.days[3].standardMenu}</span><br /><br />
                    <button class="btn" id="infoBtn">
                            <i class="fa-solid fa-circle-info"></i>
                        </button>
                </td>
                <td>
                    Tagessuppe<br /><br /><span class="menuName">${weekMenu.days[3].veggyMenu}</span><br /><br />
                    <button class="btn" id="infoBtn">
                            <i class="fa-solid fa-circle-info"></i>
                        </button>
                </td>
                <td>
                    <div>
                        <select class="custom-select custom-select-lg md-3" id="category">
                                <option value="1" selected>Menü 1</option>
                                <option value="2">Menü 2</option>
                            </select>
                    </div>
                    <div>
                        <button class="btn btn-success btn-lg btn-block" id="addBtn">
                                <i class="fa-solid fa-cart-plus"></i>
                            </button>
                    </div>
                </td>
            </tr>
            <tr valign="top" class="menu" data-id="${4}">
                <td>
                    <p>Freitag,<br /><span class="date">${weekMenu.days[4].date}</span></p>
                </td>
                <td>
                    Tagessuppe<br /><br /><span class="menuName">${weekMenu.days[4].standardMenu}</span><br /><br />
                    <button class="btn" id="infoBtn">
                            <i class="fa-solid fa-circle-info"></i>
                        </button>
                </td>
                <td>
                    Tagessuppe<br /><br /><span class="menuName">${weekMenu.days[4].veggyMenu}</span><br /><br />
                    <button class="btn" id="infoBtn">
                            <i class="fa-solid fa-circle-info"></i>
                        </button>
                </td>
                <td>
                    <div>
                        <select class="custom-select custom-select-lg md-3" id="category">
                                <option value="1" selected>Menü 1</option>
                                <option value="2">Menü 2</option>
                            </select>
                    </div>
                    <div>
                        <button class="btn btn-success btn-lg btn-block" id="addBtn">
                                <i class="fa-solid fa-cart-plus"></i>
                            </button>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>`;
}