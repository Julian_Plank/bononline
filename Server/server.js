const express = require('express')
const app = express()
const port = 3000

const bodyParser = require('body-parser')
const BonOnlineDAO = require('./BonOnlineDAO')
const BonOnlineRoutes = require('./BonOnlineRoutes')
const BonOnlineController = require("./BonOnlineController")
const DBConnection = require('./DBConnection');

const cors = require('cors')
let corsOptions = {origin: '*',  
                   optionsSuccessStatus: 200}
app.use(cors(corsOptions));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

function BonOnlineServer() {
    const dbConnection = new DBConnection();
    this.start = function() {
        dbConnection.connect().then(dbo => 
            {
                const mongodao = new BonOnlineDAO(dbo);
                const bononlinecontroller = new BonOnlineController(mongodao);
                const bononlineroutes = new BonOnlineRoutes(app, bononlinecontroller);
                bononlineroutes.initRoutes();
                app.listen(port, () => console.log('Listening on port ' + port))
            });
    }
}
let bononlineserver = new BonOnlineServer();
bononlineserver.start();