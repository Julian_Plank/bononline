module.exports = DBConnection;

let Mongo = require('mongodb').MongoClient;

let url = 'mongodb+srv://4ahif:!SuperSecret!@bononline.di3gf.mongodb.net/BonOnline?retryWrites=true&w=majority';

let database = "BonOnline"

function DBConnection(){
    this.connect = function(){
        return new Promise((resolve , reject) => {     
            Mongo.connect(url, {useNewUrlParser: true}, function(err, client) {
                if (err) throw err;
                let dbo = client.db(database);
                resolve(dbo);
            });
        });
    }
}