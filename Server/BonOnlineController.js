let ObjectID = require('mongodb').ObjectID;
const sendEmail = require('./sendEmail')

module.exports = function BonOnlineController(dao) {
    this.WebUntisAuth = require("../Login/web-untis-auth");
    this.bodao = dao;
    this.addOrder = async function(req, res) {
        let data = req.body;
        let user = await this.bodao.readOneUser(req, res, await this.WebUntisAuth.getUsernameFromSessionID(data.sessionid));
        data.customerNr = user._id;
        let count = (await this.bodao.getDocumentCount(req, res, "Orders")) + 1;
        data.pickUpNr = count;
        data.invoiceNr = Number(String(new Date().getFullYear()) + count);

        let emaildata = {};

        for (let key in data) {
            emaildata[key] = data[key];
        }

        delete data.sessionid;

        let id = (await this.bodao.addData(req, res, "Orders", data))._id;
        emaildata._id = id;
        this.sendEmail(emaildata);
        res.end("{status: success}");
    }
    this.getOrders = function(req, res) {
        this.bodao.readAllData(req, res, "Orders")
            .then(result => res.end(JSON.stringify(result)))
            .catch(err => console.log(err));
    }
    this.getOrdersFromUser = function(req, res) {
        let customerNr = req.params.customerNr;
        this.bodao.readOrdersFromUser(req, res, "Orders", customerNr)
            .then(result => res.end(JSON.stringify(result)))
            .catch(err => console.log(err));
    }
    this.deleteOrder = function(req, res) {
        let customerNr = req.params.customerNr;
        this.bodao.deleteOneData(req, res, "Orders", customerNr)
            .then(result => res.end(JSON.stringify(result)))
            .catch(err => console.log(err));
    }
    this.updateOrder = function(req, res) {
        let data = req.body;
        data.orderId = req.params.id;
        console.log(data);
        this.bodao.updateStatusInOrder(req, res, data)
            .then(result => res.end(JSON.stringify(result)))
            .catch(err => console.log(err));
    }

    this.addProduct = function(req, res) {
        let data = req.body;
        this.bodao.addData(req, res, "Produkte", data)
            .then(result => res.end(result.toString()))
            .catch(err => console.log(err));
    }
    this.getProducts = function(req, res) {
        this.bodao.readAllData(req, res, "Produkte")
            .then(result => res.end(JSON.stringify(result)))
            .catch(err => console.log(err));
    }
    this.getProduct = function(req, res) {
        this.bodao.readOneData(req, res, "Produkte", parseInt(req.params.id))
            .then(result => res.end(JSON.stringify(result)))
            .catch(err => console.log(err));
    }

    this.getWeekMenu = function(req, res) {
        this.bodao.readOneData(req, res, "Wochenmenü", parseInt(req.params.calenderWeek))
            .then(result => res.end(JSON.stringify(result)))
            .catch(err => console.log(err));
    }

    this.updateBasket = function(req, res) {
        let data = req.body;
        data.username = this.WebUntisAuth.getUsernameFromSessionID(data.sessionid);
        this.bodao.updateBasketByUserName(req, res, data)
            .then(result => res.end(JSON.stringify(result)))
            .catch(err => console.log(err));
    }
    this.updateFavourites = function(req, res) {
        let data = req.body;
        data.username = this.WebUntisAuth.getUsernameFromSessionID(data.sessionid);
        this.bodao.updateFavouritesByUserName(req, res, data)
            .then(result => res.end(JSON.stringify(result)))
            .catch(err => console.log(err));
    }
    this.updateBalance = function(req, res) {
        let data = req.body;
        data.username = this.WebUntisAuth.getUsernameFromSessionID(data.sessionid);
        this.bodao.updateBalanceByUserName(req, res, data)
            .then(result => res.end(JSON.stringify(result)))
            .catch(err => console.log(err));
    }
    this.getUser = function(req, res) {
        let id = req.params.sessionID;
        let username = this.WebUntisAuth.getUsernameFromSessionID(id);
        this.bodao.readOneUser(req, res, username)
            .then(result => res.end(JSON.stringify(result)))
            .catch(err => console.log(err));
    }

    this.login = async function(req, res) {
        let username = req.body.username;
        let password = req.body.password;

        let result = await this.WebUntisAuth.login(username, password);
        if (result) {
            this.bodao.checkIfUserExists(req, res, username)
                .then(exists => {
                    if (!exists) {
                        this.bodao.addData(req, res, "User", { favourites: [], balance: 0, shoppingbasket: [], username: username })
                            .then(res.send(this.WebUntisAuth.getSessionToken(username)))
                            .catch(err => console.log(err));
                    } else {
                        res.send(this.WebUntisAuth.getSessionToken(username));
                    }
                })
                .catch(err => console.log(err));
        } else {
            res.sendStatus(400);
        }
    }
    this.logout = async function(req, res) {
        let sessionid = req.params.sessionid;
        console.log("Session ID:", sessionid);

        let result = await this.WebUntisAuth.logout(this.WebUntisAuth.getUsernameFromSessionID(sessionid));

        if (result) {
            //Wenn der Login funktioniert, schickt der Server dem Client noch einen String zurück, den dieser
            //dann document.cookie zuweisen muss, damit das Cookie gelöscht wird.
            res.send(`JSESSIONID=${sessionid}; Path=/; Secure; SameSite=None; Expires=${new Date(0).toUTCString()}`);
        } else {
            res.sendStatus(400);
        }
    }
    this.getUserData = async function(req, res) {
        let result = this.getData(req.params.username);
        res.send(JSON.stringify(result));
    }
    this.getUsername = function(req, res) {
        let id = req.params.sessionID;

        let result = this.WebUntisAuth.getUsernameFromSessionID(id);

        if (result) {
            res.send(result);
        } else {
            res.send(false);
        }
    }
    this.sendEmail = async function(result) {
        let temp = await this.getData(this.WebUntisAuth.getUsernameFromSessionID(result.sessionid));
        result.email = temp.email;
        result.name = temp.name;
        result.gender = temp.gender;
        sendEmail(result);
    }
    this.getData = async function(username) {
        let result = {
            email: await this.WebUntisAuth.getUserEmail(username),
            name: await this.WebUntisAuth.getUserFullName(username),
            gender: await this.WebUntisAuth.getGender(username)
        }
        return result;
    }
    //Die Funktion ist nicht mehr notwendig, siehe functions.js in helper.
    /*
    this.logincheck = async function(req, res) {
        let sessionid = req.body.sessionid;
        res.send(this.WebUntisAuth.isLoggedIn(this.WebUntisAuth.getUsernameFromSessionID(sessionid)));
    }
    */
}