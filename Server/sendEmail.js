let http = require('http');
let url = require('url');
const puppeteer = require('puppeteer');
var nodemailer = require('nodemailer'); 
const { readFileSync } = require('fs');

function generateHtml(orderNumber, name, email, products, deliverydate, orderdate, customerNr, gender, pickUpNr, invoiceNr) {
  let sum = calcSum(products);
  let tax = calcTax(products);
  let tableBody = generateTable(products);
  let lastname = name.split(' ')[1];
  let genderText = (gender.toUpperCase().localeCompare("MALE") == 0) ? "r Herr " + lastname : " Frau " + lastname;
  let html = `<!DOCTYPE html>
  <html lang="en">
  <head>
      <link rel="icon" type="image/x-icon" href="./aceofhearts.png">
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Rechnung</title>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
    <script src="test.js" type="module"></script>
    <script src="https://raw.githack.com/eKoopmans/html2pdf/master/dist/html2pdf.bundle.js"></script>
    <script src="https://smtpjs.com/v3/smtp.js"></script>
  </head>
  <body>
    <div class="container text-center">
    <img src="data:image/jpeg;base64,${
      readFileSync('./logo_small.png').toString('base64')
    }" alt="alt text" />
    </div>
    <div class="float-left">
      <div class="row">
        <div class="app border border-dark rounded m-md-5 p-md-5 m-2 p-3 bg-light">
          <p id="name">${name}</p>
          <p id="email">${email}</p>
        </div>
      </div>
    </div>
    <div class="float-right">
      <div class="row">
        <div class="app border border-dark rounded m-md-5 p-md-5 m-2 p-3 bg-light">
          <p id="abholung">Abholung: ${deliverydate.split("T")[0] + " " + deliverydate.split("T")[1]}</p>
          <p id="bestellzeitpunkt">Bestellung: ${orderdate.split("T")[0] + " " + orderdate.split("T")[1]}</p>
          <p id="kundennr">KundenNr: ${customerNr}</p>
          <p id="bestellnr">BestellNr: ${orderNumber}</p>
          <p id="abholnr">AbholNr: ${pickUpNr}</p>
          <p id="rechnungsnr">RechnungsNr: ${invoiceNr}</p>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="float-left">
        <p>Sehr geehrte${genderText},</p>
        <p>Wir bedanken uns bei Ihnen für Ihre Bestellung. Hiermit stellen wir Ihnen folgende Produkte in Rechnung:</p>
      </div>
      <table class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">Pos.</th>
            <th scope="col">Beschreibung</th>
            <th scope="col">Menge</th>
            <th scope="col">Einzelpreis</th>
            <th scope="col">Gesamtpreis</th>
          </tr>
        </thead>
        <tbody id="table_product_body">
          ${tableBody}
          <tr>
            <th></th>
            <td></td>
            <td></td>
            <th scope="row">Netto</td>
            <td scope="row">${sum} €</td>
          </tr>
          <tr>
          <th></th>
          <td></td>
          <td></td>
          <td>+ 20%</td>
          <td scope="row">${tax} €</td>
        </tr>
          <tr>
            <th></th>
            <td></td>
            <td></td>
            <th scope="row">Brutto</td>
            <td scope="row">${Number(sum)+Number(tax)} €</td>
          </tr>
        </tbody>
      </table>      
        <p>Bei Rückfragen stehen wir selbstverständlich jederzeit gerne zur Verfügung.</p>
        <br>
        <p>Mit freundlichen Grüßen</p>
        <p>Ihr BonOnline-Team.</p>
      
    </div>
  </body>
  </html>`;
  return html;
}
function generateTable(products) {
  let newProducts = convertProducts(products);
  let html = "";
  let i = 1;
  newProducts.forEach(element => {
    html += `<tr>
      <th scope="row">${i}</th>
      <td>${element.description}</td>
      <td>${element.count}</td>
      <td>${element.price}</td>
      <td>${element.price * element.count} €</td>
    </tr>`;
    i += 1;
  });
  return html;
}
function convertProducts(products) {
  let list = new Array();
  products.forEach(element => {
    let obj = {};
    obj.count = element.quantity;
    obj.description = element.name;
    obj.price = Number(element.price).toFixed(2);
    obj.tax =Number(element.tax).toFixed(2);
    list.push(obj);
  });  
  return list;
}
function calcSum(products) {
  let sum = 0;
  products.forEach(element => {
    sum += Number(element.price)*element.quantity;
  });
  return sum;
}
function calcTax(products, tax) {
  let sum = 0;
  products.forEach(element => {
    sum += Number(element.tax)*element.quantity;
  });
  return sum.toFixed(2);
}

module.exports = async function generateEmailAndSendPdf(obj) {
    let lastname = obj.name.split(' ')[1];
    let genderText = (obj.gender.toUpperCase().localeCompare("MALE") == 0) ? "r Herr " + lastname : " Frau " + lastname;
    let html = generateHtml(obj._id, obj.name, obj.email, obj.products, obj.deliverydate, obj.orderdate, obj.customerNr, obj.gender, obj.pickUpNr, obj.invoiceNr);
    try {
        const browser = await puppeteer.launch({ args: ['--allow-file-access-from-files', '--enable-local-file-accesses'] });
        const page = await browser.newPage();

        await page.setContent(html);
        pdf = await page.pdf({
            path: 'rechnung.pdf',
            format: 'A4',
            printBackground: true
        });
        console.log("done");
        await browser.close();
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            Host: "smtp.gmail.com",
            secure: false, 
            port: 25, 
            tls: {
                rejectUnauthorized: false
            },
            auth: {
              user: 'bononlinetest@gmail.com',
              pass: 'Test123!'
            }
          });
        var mailOptions = {
          from: 'bononlinetest@gmail.com',
          to: obj.email,
          subject: 'Rechnung',
          text: 'Sehr geehrte' + genderText + '!\n\nHiermit bedanken wir uns recht herzlich bei Ihnen für Ihre Bestellung. Anbei finden Sie die Rechnung.\n\nMit freundlichen Grüßen\nIhr BonOnline-Team',
          attachments: [
            {  
                filename: 'rechnung.pdf',
                content: pdf
            }
          ]
        };
        transporter.sendMail(mailOptions, function(error, info){
          if (error) {
            console.log(error);
          } else {
            console.log('Email sent: ' + info.response);
          }
        }); 
    } catch(e) {
        console.log(e);
    }
}
