"use strict"

let ObjectID = require('mongodb').ObjectID;

module.exports = function BonOnlineDAO(db) {
    this.dbo = db;
    this.readAllData = function(req, res, collection) {
        return new Promise((resolve, reject) => {
            this.dbo.collection(collection).find({}).toArray(function(err, result) {
                if (err) reject(err);
                resolve(result);
            });
        });
    }
    this.readOneData = function(req, res, collection, id) {
        return new Promise((resolve, reject) => {
            this.dbo.collection(collection).findOne({ _id: id }, function(err, result) {
                if (err) reject(err);
                resolve(result);
            });
        });
    }
    this.readOrdersFromUser = function(req, res, collection, customerNr) {
        return new Promise((resolve, reject) => {
            this.dbo.collection(collection).find({ customerNr: new ObjectID(customerNr) }).toArray(function(err, result) {
                if (err) reject(err);
                resolve(result);
            });
        });
    }

    this.deleteOneData = function(req, res, collection, id) {
        return new Promise((resolve, reject) => {
            this.dbo.collection(collection).deleteOne({ _id: new ObjectID(id) }, function(err, result) {
                if (err) throw err;
                resolve(result);
            });
        });

    }

    this.addData = function(req, res, collection, data) {
        return new Promise((resolve, reject) => {
            this.dbo.collection(collection).insertOne(data, function(err, result) {
                if (err) throw err;
                resolve(data);
            });
        });
    }
    this.updateData = function(req, res, collection, data) {
        return new Promise((resolve, reject) => {
            this.dbo.collection(collection).findOneAndUpdate({ _id: data._id }, data, { new: true },
                function(err, result) {
                    if (err) throw err;
                    resolve(result);
                }
            );
        });
    }
    this.updateBasketByUserName = function(req, res, data) {
        return new Promise((resolve, reject) => {
            this.dbo.collection("User").findOneAndUpdate({ username: data.username }, {
                    $set: {
                        shoppingbasket: data.basket
                    }
                }, { new: true },
                function(err, result) {
                    if (err) reject(err);
                    resolve(result);
                }
            );
        });
    }
    this.updateStatusInOrder = function(req, res, data) {
        console.log(data);
        return new Promise((resolve, reject) => {
            this.dbo.collection("Orders").findOneAndUpdate({ _id: new ObjectID(data.orderId) }, {
                    $set: {
                        status: data.status
                    }
                }, { new: true },
                function(err, result) {
                    if (err) reject(err);
                    resolve(result);
                }
            );
        });
    }
    this.updateBalanceByUserName = function(req, res, data) {
        return new Promise((resolve, reject) => {
            this.dbo.collection("User").findOneAndUpdate({ username: data.username }, {
                    $set: {
                        balance: data.balance
                    }
                }, { new: true },
                function(err, result) {
                    if (err) reject(err);
                    resolve(result);
                }
            );
        });
    }

    this.readOneUser = function(req, res, username) {
        return new Promise((resolve, reject) => {
            this.dbo.collection("User").findOne({ username: username }, function(err, result) {
                if (err) reject(err);
                resolve(result);
            });
        });
    }
    this.updateFavouritesByUserName = function(req, res, data) {
        return new Promise((resolve, reject) => {
            this.dbo.collection("User").findOneAndUpdate({ username: data.username }, {
                    $set: {
                        favourites: data.favourites
                    }
                }, { new: true },
                function(err, result) {
                    if (err) reject(err);
                    resolve(result);
                }
            );
        });
    }
    this.checkIfUserExists = function(req, res, usern) {
        return new Promise((resolve, reject) => {
            resolve(this.dbo.collection("User").find({ username: usern }).count());
        });
    }
    this.getDocumentCount = function(req, res, collection) {
        return new Promise((resolve, reject) => {
            resolve(this.dbo.collection(collection).countDocuments());
        });
    }
}