module.exports = function BonOnlineRoutes(app, controller) {
    this.app = app;
    this.controller = controller;
    this.initRoutes = function() {
        this.app.route("/products")
            .get((req, res) => {
                //Get all products here
                this.controller.getProducts(req, res);
            })
            .post((req, res) => {
                //Add Product here
                this.controller.addProduct(req, res);
            })

        this.app.route("/products/:id")
            .get((req, res) => {
                //Get one product here
                this.controller.getProduct(req, res);
            })
            .delete((req, res) => {
                //Delete one product here
                this.controller.deleteProduct(req, res);
            })
            .put((req, res) => {
                //Update one product here
                this.controller.updateProduct(req, res);
            })

        this.app.route("/orders")
            .get((req, res) => {
                //Get all orders
                this.controller.getOrders(req, res);
            })
            .post((req, res) => {
                //Add a order
                this.controller.addOrder(req, res);
            })
        this.app.route("/orders/:id")
            .put((req, res) => {
                this.controller.updateOrder(req, res);
            })


        this.app.route("/orders/:customerNr")
            .get((req, res) => {
                this.controller.getOrdersFromUser(req, res);
            })
            .delete((req, res) => {
                this.controller.deleteOrder(req, res);
            })

        this.app.route("/weekMenu/:calenderWeek")
            .get((req, res) => {
                this.controller.getWeekMenu(req, res);
            })
            .post((req, res) => {
                this.controller.addWeekMenu(req, res);
            })

        this.app.route("/login")
            .post((req, res) => {
                this.controller.login(req, res);
            })
        //Nicht mehr notwendig. Siehe functions.js in helper.
        /*
        this.app.route("/login/check")
            .post((req, res) => {
                this.controller.logincheck(req, res);
            })
            */
        this.app.route("/logout/:sessionid")
            .get((req, res) => {
                this.controller.logout(req, res);
            })
        this.app.route("/username/:sessionID")
            .get((req, res) => {
                this.controller.getUsername(req, res);
            })
        this.app.route("/email-data/:username")
            .get((req, res) => {
                this.controller.getUserData(req, res);
            })

        this.app.route("/users/:sessionID")
            .get((req, res) => {
                this.controller.getUser(req, res);
            })
        this.app.route("/users/basket")
            .put((req, res) => {
                this.controller.updateBasket(req, res);
            })
        this.app.route("/users/favourites")
            .put((req, res) => {
                this.controller.updateFavourites(req, res);
            })
        this.app.route("/users/balance")
            .put((req, res) => {
                this.controller.updateBalance(req, res);
            })
    }
};