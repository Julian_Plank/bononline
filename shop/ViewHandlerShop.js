"use strict";

import { ge, calculateTotal } from "../Helper/functions.js";
import { ViewHandlerHeader } from "../Helper/ViewHandlerHeader.js";
import { createElementFromHtml } from "../Helper/DomUtils.js";
import { renderProduct } from "../Templates/Product.template.js";
import { renderMenu } from "../Templates/Menu.template.js";
import { Modal } from "../Helper/Modal.js";
import template from "../Templates/LastOrder.template.js";
import { renderProductForOrder } from "../Templates/ProductLastOrder.template.js";
import { ViewHandlerFooter } from "../Helper/ViewHandlerFooter.js";

export function ViewHandlerShop() {
    this.vhHeader = new ViewHandlerHeader();
    this.footer = new ViewHandlerFooter();
    this.productsElement = ge("products");
    this.lastOrder = ge("lastOrder");
    this.modal = new Modal();
}

ViewHandlerShop.prototype.renderFavourites = function(favourites) {
    this.productsElement = ge("products");
    let list = document.createElement("div");
    list.className = "row";
    list.id = "products";
    let id = 0;

    let div = document.createElement("div");
    div.className = "noFavourites";
    div.id = "products";

    if (favourites.length != 0) {
        for (let product of favourites) {
            let productElem = renderProduct(product);
            let tmp = document.createElement("template");
            tmp.innerHTML = `<p><button id="addToShoppingBasket" data-addidx="${id}" class="btn btn-success"><i class="fa-solid fa-cart-plus"></i></button>
        <button id="deleteFavourite" data-delidx="${id}" class="btn btn-danger"><i class="fa-solid fa-x"></i></button></p>`;
            productElem.querySelector("h5").after(tmp.content);
            list.append(productElem);
            id++;
        }
        this.productsElement.replaceWith(list);
        this.productsElement = this.productsElement.list;
    } else {
        div.innerHTML = "Keine Favoriten vorhanden!";
        this.productsElement.replaceWith(div);
    }
};

ViewHandlerShop.prototype.bindAddShoppingBasket = function(callback) {
    let addBtns = document.querySelectorAll("#addToShoppingBasket");
    for (let btn of addBtns) {
        btn.onclick = () => {
            this.modal.modifyBody("Produkt wurde zum Warenkorb hinzugefügt! Zum Warenkorb gehen?");
            this.modal.container.querySelector("#yes").onclick = function() {
                window.location.href = "/ShoppingBasket/ShoppingBasket.html";
            };
            callback(btn.dataset.addidx);
            jQuery(this.modal.container).modal("show");
        };
    }
};
ViewHandlerShop.prototype.renderLastOrder = function(lastOrder) {
    this.lastOrder = ge("lastOrder");
    let app = document.createElement("div");
    let list = document.createElement("div");
    let id = 0;

    app.className = "app border rounded m-md-5 p-md-5 m-2 p-3 border";
    app.id = "app";
    list.className = "row";
    list.id = "lastOrder";

    let div = document.createElement("div");
    div.className = "noLastOrder";
    div.id = "lastOrder";

    if (lastOrder != undefined) {
        for (let product of lastOrder.products) {
            console.log(product);
            let productElem;
            product.type == "product" ? (productElem = renderProductForOrder(product)) : (productElem = renderMenu(product));
            list.append(productElem);
            id++;
        }
        let total = createElementFromHtml(template(lastOrder.sum));
        app.append(list);
        this.lastOrder.replaceWith(app);
        this.lastOrder = this.lastOrder.app;
        document.querySelector("h3[style='text-align: left']").nextSibling.replaceWith(total);
        this.checkLastOrderForMenu(lastOrder);
    } else {
        div.innerHTML = "Keine letzte Bestellung vorhanden!"
        this.lastOrder.replaceWith(div);
    }
};

ViewHandlerShop.prototype.bindDeleteFavourite = function(callback) {
    let deleteBtns = document.querySelectorAll("#deleteFavourite");
    for (let btn of deleteBtns) {
        btn.onclick = function() {
            callback(btn.dataset.delidx);
        };
    }
};

ViewHandlerShop.prototype.checkLastOrderForMenu = function(lastOrder) {
    let check = false;
    if (lastOrder != undefined) {
        for (let product of lastOrder.products) {
            console.log(product);
            if (product.type.startsWith("menu")) {
                check = true;
            }
        }
        check ? this.setTooltipAndDisableTrueButton() : this.removeTooltipAndDisableFalseButton();
    }
};

ViewHandlerShop.prototype.setTooltipAndDisableTrueButton = function() {
    let btn = document.querySelector("#oneClick");
    btn.disabled = true;
    btn.setAttribute("data-toggle", "tooltip");
    btn.setAttribute("data-placement", "top");
    btn.setAttribute("title", "Bezhalung nicht möglich, da ein Menü vorhanden ist!");
};

ViewHandlerShop.prototype.removeTooltipAndDisableFalseButton = function() {
    let btn = document.querySelector("#oneClick");
    btn.disabled = false;
    btn.removeAttribute("data-toggle");
    btn.removeAttribute("data-placement");
    btn.removeAttribute("title");
};

ViewHandlerShop.prototype.bindOneClickOrder = function(callback) {
    let oneClickBtn = document.querySelector("#oneClick");
    if (oneClickBtn != undefined) {
        oneClickBtn.onclick = () => {
            this.modal.modifyBody(`Möchten Sie Ihre letzte Bestellung erneut bestellen?`);
            this.modal.container.querySelector(".modal-footer").innerHTML = "";
            this.modal.container.querySelector(".modal-footer").innerHTML =
                '<button type="button" class="btn btn-secondary" data-dismiss="modal">Nein</button><button type="button" id="yes" class="btn btn-success" data-dismiss="modal">Ja</button>';
            this.modal.container.querySelector("#yes").onclick = () => {
                callback();
                window.location.href = "/Checkout/Checkout.html";
            };
            jQuery(this.modal.container).modal("show");
        };
    }
};