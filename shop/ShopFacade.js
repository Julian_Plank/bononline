import { UserDAO } from "../UserDAO.js";
import { ViewHandlerShop } from "./ViewHandlerShop.js";
import { readCookie, checkIfProductAlreadyExists, modifyAmount } from "../Helper/functions.js";
import { validateAuth } from "../helper/functions.js";
import { OrderDAO } from "../OrderDAO.js";

function ShopFacade() {
	validateAuth();
	this.userDAO = new UserDAO();
	this.orderDAO = new OrderDAO();
	this.vH = new ViewHandlerShop();
	this.favourites = [];
	this.user = undefined;
	this.lastOrder = undefined;
}

ShopFacade.prototype.loadUserData = function () {
	this.userDAO.loadUser((user) => {
		this.user = JSON.parse(user);
		this.favourites = this.user.favourites;
		this.updateView();
	}, readCookie("JSESSIONID"));
};

ShopFacade.prototype.loadLastOrder = function () {
	this.orderDAO.loadOrdersFromUser((orders) => {
		this.lastOrder = JSON.parse(orders)[JSON.parse(orders).length - 1];
		console.log(this.lastOrder);
		this.vH.renderLastOrder(this.lastOrder);
		this.lastOrderOneClick(this.lastOrder);
	}, this.user._id);
};

ShopFacade.prototype.updateView = function () {
	this.vH.renderFavourites(this.favourites);
	this.loadLastOrder();
	this.removeFavourite();
	this.addFavouriteToShoppingBasket();
};

ShopFacade.prototype.removeFavourite = function () {
	this.vH.bindDeleteFavourite((id) => {
		this.favourites.splice(id, 1);
		this.userDAO.updateFavourites((data) => {}, { sessionid: document.cookie.split("=")[1], favourites: this.favourites });
		this.updateView();
	});
};

ShopFacade.prototype.addFavouriteToShoppingBasket = function () {
	this.vH.bindAddShoppingBasket((id) => {
		this.favourites[id]["amount"] = 1;
		if (checkIfProductAlreadyExists(this.favourites[id], this.user.shoppingbasket)) {
			modifyAmount(this.user.shoppingbasket, this.favourites[id]);
		} else {
			this.user.shoppingbasket.push(this.favourites[id]);
		}
		this.userDAO.updateShoppingBasket((data) => {}, { sessionid: document.cookie.split("=")[1], basket: this.user.shoppingbasket });
	});
};

ShopFacade.prototype.lastOrderOneClick = function (lastOrder) {
	this.vH.bindOneClickOrder(() => {
		let products = [];
		console.log(lastOrder);
		for (let p of lastOrder.products) {
			let object = {};
			p.price = String(Number(p.price) + Number(p.tax));
			let total;
			if (p.price.indexOf(".") != -1) {
				total = p.price.split(".");
				p.price = total[0] + "," + total[1];
			}
			let indexOfComma = p.price.indexOf(",");
			indexOfComma == -1 ? (p.price += ",00€") : (p.price += "0€");
			object["price"] = p.price;
			object["name"] = p.name;
			object["amount"] = p.quantity;
			object["description"] = p.description;
			products.push(object);
			console.log(products);
		}
		console.log(products);
		localStorage["shoppingbasket"] = JSON.stringify(products);
		this.userDAO.updateShoppingBasket((data) => {}, { sessionid: document.cookie.split("=")[1], basket: products });
	});
};

ShopFacade.prototype.init = function () {
	this.vH.vhHeader.createHeader(true, false, false, false, false);
	this.loadUserData();
};

let f = new ShopFacade();
f.init();
