"use strict";

import { ViewHandlerHeader } from "../Helper/ViewHandlerHeader.js";
import { Modal } from "../Helper/Modal.js";
import { ViewHandlerFooter } from "../Helper/ViewHandlerFooter.js";


export function ViewHandlerPrivacy() {
	this.vhHeader = new ViewHandlerHeader();
	this.footer = new ViewHandlerFooter();
	this.modal = new Modal();
}

ViewHandlerPrivacy.prototype.init = function() {
    this.vhHeader.createHeader(false, false, false, false, false);
}

let viewHandlerPrivacy = new ViewHandlerPrivacy().init();
