import { BaseDAO } from "./BaseDAO.js";

export function UserDAO() {
    this.baseDAO = new BaseDAO();
}

UserDAO.prototype.updateShoppingBasket = function(callback, data) {
    let xhr = this.baseDAO.prepareRequest("PUT", `/users/basket`, {});
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = () => callback(xhr.response);
    xhr.send(JSON.stringify(data));
}

UserDAO.prototype.updateFavourites = function(callback, data) {
    let xhr = this.baseDAO.prepareRequest("PUT", `/users/favourites`, {});
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = () => callback(xhr.response);
    console.log(data);
    xhr.send(JSON.stringify(data));
}

UserDAO.prototype.updateBalance = function(callback, data) {
    let xhr = this.baseDAO.prepareRequest("PUT", `/users/balance`, {});
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = () => callback(xhr.response);
    console.log(data);
    xhr.send(JSON.stringify(data));
}

UserDAO.prototype.loadShoppingBasket = function(callback) {
    let xhr = this.baseDAO.prepareRequest("GET", "/users/basket", {});
    xhr.onload = () => callback(xhr.response);
    xhr.send();
}

UserDAO.prototype.loadUser = function(callback, sessionID) {
    let xhr = this.baseDAO.prepareRequest("GET", `/users/${sessionID}`, {});
    xhr.onload = () => callback(xhr.response);
    xhr.send();
}

UserDAO.prototype.loadBalance = function(callback) {
    let xhr = this.baseDAO.prepareRequest("GET", `/users/balance`, {});
    xhr.onload = () => callback(xhr.response);
    xhr.send();
}