"use strict";

const axios = require("axios").default;

const COOKIE_LIFETIME = 600;

const baseUrl = "https://hektor.webuntis.com/";
const schoolname = "testpink";
const cookieBuilder = require("cookie");

const WebUntisAuth = new function(){
    const users = new Map();

    const isStatusValid = status => status >= 200 && status <= 226;

    const containsError = res => res.data.error !== undefined;

    const handleError = function(res, onSuccessCallback){
        if(containsError(res) || !isStatusValid(res.status)){
            console.log(new Error(`Message: ${res.data.error.message}, error code: ${res.data.error.code}`));
            return false;
        } else {
            onSuccessCallback();
            return true;
        }
    }

    this.login = async function(username, password){
        if(!password){
            console.log(new Error("No password given!"));
            return false;
        }

        const id = username;
        const res = await axios.post(`${baseUrl}WebUntis/jsonrpc.do?school=${schoolname}`, {
            id: id,
            method: "authenticate",
            params: {
                user: username,
                password: password,
                client: id
            },
            jsonrpc: "2.0"
        });

        const JSESSIONID = cookieBuilder.parse(
            res.headers["set-cookie"].join(";")
            ).JSESSIONID;

        const now = new Date();
        const duration = COOKIE_LIFETIME * 1000;
        const expiresAt = new Date(now.getTime() + duration);
        console.log(`JSESSIONID=${JSESSIONID}; Path=/; Secure; SameSite=None; Expires=${expiresAt.toUTCString()}`);

        return handleError(res, () => 
            users.set(id, {
                schoolname: schoolname,
                sessionData: res.data.result,
                cookie: `JSESSIONID=${JSESSIONID}; Path=/; Secure; SameSite=None; Expires=${expiresAt.toUTCString()}`
            })
        );
    }

    this.getSessionToken = function(username){
        return users.get(username)?.cookie;
    }

    this.logout = async function(username){
        if(this.isLoggedIn(username)){
            const id = username;
            const user = users.get(id);
            const res = await axios.post(`${baseUrl}WebUntis/jsonrpc.do?school=${schoolname}`, {
                id: id,
                method: "logout",
                params: {},
                jsonrpc: "2.0"
            }, {
                headers: {
                    Cookie: user.cookie
                }
            });

            return handleError(res, () => users.delete(id));
        } else{
            console.log(new Error("User is not logged in"));
            return false;
        }
    }

    const getUserDataForPersonType = async (method, username, cookie) => {
        return axios.post(`${baseUrl}WebUntis/jsonrpc.do?school=${schoolname}`, {
            id: username,
            method: method,
            params: {},
            jsonrpc: "2.0"
        }, {
            headers: {
                Cookie: cookie
            }
        });
    }

    this.getUserFullName = async function(username){
        if(users.has(username)){
            const user = users.get(username);
            console.log(user.cookie);
            const res = await getUserDataForPersonType(user.sessionData.personType === this.TYPES.STUDENT ? "getStudents" : "getTeachers",
            username, user.cookie);
            const person = res.data.result.find(p => p.id === user.sessionData.personId);
            return person ? person.foreName + ' ' + person.longName : undefined;
        } else {
            return undefined;
        }
    }

    this.getUserEmail = async function(username){
        if(this.isLoggedIn(username)){
            const user = users.get(username);
            const res = await axios.get(`${baseUrl}WebUntis/api/profile/general`, {
                headers:{
                    Cookie: user.cookie
                }
            });
            console.log(res.data);
            console.log(res.data.data.profile.gender);
            return res.data ? res.data.data.profile.email : undefined;
        }
    }

    const getGenderString = genderLabelString => {
        console.log(genderLabelString);
        switch(genderLabelString){
            case "LBL_MALE": return "male";
            case "LBL_FEMALE": return "female";
            case "LBL_GENDER_INTER_DIVERS": return "divers";
            default:
                return undefined;
        }
    }

    this.getGender = async function(username){
        if(this.isLoggedIn(username)){
            const user = users.get(username);
            const res = await axios.get(`${baseUrl}WebUntis/api/profile/general`, {
                headers: {
                    Cookie: user.cookie
                }
            });
            console.log(res.data.data.profile);
            return res.data ? getGenderString(res.data.data.profile.gender?.longLabel) : undefined;
        }
    }

    this.isLoggedIn = username => users.has(username);

    this.getUserInfo = username => users.get(username).sessionData;

    this.getUsernameFromSessionID = sessionID => {
        for (const [username, data] of users) {
            if(data.sessionData.sessionId == sessionID){
                return username;
            }
        }
        return undefined;
    }

    this.TYPES = {
        ADMIN: 16,
        TEACHER: 2,
        STUDENT: 5,
    };
    
    Object.freeze(this.TYPES);
};

module.exports = WebUntisAuth;