const WebUntisAuth = require("./web-untis-auth");
const express = require('express');
const app = express();
app.use(express.json());
const port = 3000;

const cors = require('cors');
app.use(cors({
    origin: '*'
}));

app.post('/login', async function(req, res){
    console.log("/login");
    const username = req.body.username;
    const password = req.body.password;

    const result = await WebUntisAuth.login(username, password);

    console.log("Result:", result);
    if(result){
        res.send(WebUntisAuth.getSessionToken(username));
    }else{
        res.sendStatus(400);
    }
});

app.get('/logout/:username', async function(req, res){
    console.log("/logout");

    const username = req.params.username;

    const result = await WebUntisAuth.logout(username);

    console.log(result);

    if(result){
        res.sendStatus(200);
    }else{
        res.sendStatus(400);
    }
});

app.get('/email-data/:username', async function(req, res){
    console.log("/email-data")
    let result = {
        email: await WebUntisAuth.getUserEmail(req.params.username),
        name: await WebUntisAuth.getUserFullName(req.params.username)
    }
    res.send(JSON.stringify(result));
});

app.listen(port, () => console.log(`Listening on port ${port}!`));