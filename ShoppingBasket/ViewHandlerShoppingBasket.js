"use strict";

import { ge } from "/helper/functions.js";
import { ViewHandlerHeader } from "../Helper/ViewHandlerHeader.js";
import { Modal } from "../Helper/Modal.js";
import { ViewHandlerFooter } from "../Helper/ViewHandlerFooter.js";

export function ViewHandlerShoppingBasket() {
    this.vhHeader = new ViewHandlerHeader();
    this.footer = new ViewHandlerFooter();
    this.modal = new Modal();
}

ViewHandlerShoppingBasket.prototype.bindDelete = function(callback) {
    let delBtn = document.querySelectorAll("#delBtn");
    for (let btn of delBtn) {
        btn.onclick = function() {
            callback(btn.dataset.delidx);
        };
    }
};

ViewHandlerShoppingBasket.prototype.bindUpdate = function(callback) {
    let updBtn = document.querySelectorAll("#updBtn");
    for (let btn of updBtn) {
        btn.onclick = () => {
            this.modal.modifyBody(`Neue Anzahl: <input type="number" min="1" value=${1} id="amount">`);
            this.modal.container.querySelector(".modal-footer").innerHTML = "";
            this.modal.container.querySelector(".modal-footer").innerHTML =
                '<button type="button" class="btn btn-secondary" data-dismiss="modal">Nein</button><button type="button" id="save" class="btn btn-success" data-dismiss="modal">Speichern</button>';
            this.modal.container.querySelector("#save").onclick = () => {
                callback(btn.dataset.updidx, this.modal.container.querySelector("#amount").value);
            };
            jQuery(this.modal.container).modal("show");
        };
    }
};

ViewHandlerShoppingBasket.prototype.renderMenu = function(menu, id) {
    let newElement = document.createElement("template");
    newElement.innerHTML = `<div class="column">
	<div class="card">
		<img class="card-img-top" src="../Assets/Images/Menu${menu.category}.jpg" alt="Card image cap">
		  <div class="card-body">
			<h3 class="card-title">Menü ${menu.category}<h3><h5>(${menu.date})</h5>
			<h5 class="card-title">${menu.price}</h5>
			<button data-delIdx="${id}" id="delBtn"class="btn btn-sm btn-outline-danger"><i class="fa fa-trash"></i> LÖSCHEN</button>
		  </div>
	</div>
  </div>`;
    return newElement.content;
};

ViewHandlerShoppingBasket.prototype.renderProduct = function(product, id) {
    console.log(product)
    let newElement = document.createElement("template");
    newElement.innerHTML = `<div class="column">
                <div class="card">
                    <img class="card-img-top" src="../Assets/Images/${product.name.replace(" ", "_")}.jpg" alt="Card image cap">
                  	<div class="card-body">
                    	<h3 class="card-title">${product.name}</h3>
                        <h5 class="form-imput">Anzahl: ${product.amount == undefined ? product.quantity : product.amount}</h5>
                        <h5 class="card-title">${product.price}</h5>
                    	<button data-delIdx="${id}" id="delBtn"class="btn btn-sm btn-outline-danger"><i class="fa fa-trash"></i> LÖSCHEN</button>
						<button data-updIdx="${id}" id="updBtn"class="btn btn-sm btn-outline-warning"><i class="fa-solid fa-pen"></i> ÄNDERN</button>
                  	</div>
                </div>
              </div>`;
    return newElement.content;
};

ViewHandlerShoppingBasket.prototype.checkForEmptyBasket = function(list) {
    if (list == 0) {
        return true;
    } else {
        return false;
    }
};

ViewHandlerShoppingBasket.prototype.renderProducts = function(shoppingBasket) {
    if (!this.checkForEmptyBasket(shoppingBasket.length)) {
        document.querySelector("#shoppingbasket").innerHTML = `<div id="totalCost">
                                                                    <h3>Gesamtsumme: € 0</h3>
                                                                </div>
                                                                <div class="overview">
                                                                    <h3>Übersicht ihrer Produkte:</h3>
                                                                    <div id="myProducts"></div>
                                                                </div>`;
        this.productsElement = ge("myProducts");
        let list = document.createElement("div");
        let app = document.createElement("div");
        let id = 0;
        let totalCost = 0;

        list.className = "row";
        list.id = "myProducts";
        app.className = "app border rounded m-md-5 p-md-5 m-2 p-3 border";
        app.id = "app";

        for (let product of shoppingBasket) {
            let productElem = product.type == "menu" ? this.renderMenu(product, id) : this.renderProduct(product, id);
            list.append(productElem);
            id++;
            totalCost += this.changeToNumber(product.price) * product.amount;
        }
        totalCost = totalCost.toFixed(2);

        app.append(list);
        this.productsElement.replaceWith(app);
        this.productsElement = this.productsElement.app;
        this.totalCost = ge("totalCost");
        this.totalCost.innerHTML = "";
        this.totalCost.append(this.createTotalCost(String(totalCost).replace(".", ",")));
        document.querySelector("#checkout").onclick = function() {
            window.location.href = "../Checkout/Checkout.html";
        };
    } else {
        document.querySelector("#shoppingbasket").innerHTML = `<div>
                                                                    <center><h2 id="myProducts">Dein Warenkorb ist leer!</h2></center>
                                                                </div>`;
    }
};

ViewHandlerShoppingBasket.prototype.changeToNumber = function(string) {
    let number = string.split("€").join("").replace(",", ".");
    return Number(number);
};

ViewHandlerShoppingBasket.prototype.createTotalCost = function(totalCost) {
    let div = document.createElement("div");
    div.className = "input-group";
    div.innerHTML = `<h3>Gesamtsumme: ${totalCost}€</h3>
        <a href="/Checkout/Checkout.html">
            <button type="button" class="btn btn-lg btn-success" id="checkout">Zur Kasse</button>
        </a>`;
    return div;
};