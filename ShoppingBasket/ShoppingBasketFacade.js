import { ViewHandlerShoppingBasket } from "./ViewHandlerShoppingBasket.js";
import { UserDAO } from "../UserDAO.js";
import { readCookie, validateAuth } from "../helper/functions.js"

export default function ShoppingBasketFacade() {
    validateAuth();
    this.userDAO = new UserDAO();
    this.user;
    this.vH = new ViewHandlerShoppingBasket();
    this.shoppingBasket = [];
}

ShoppingBasketFacade.prototype.loadShoppingBasket = function() {
    this.userDAO.loadUser(user => {
        this.shoppingBasket = JSON.parse(user).shoppingbasket;
        this.user = JSON.parse(user);
        this.updateView();
    }, readCookie("JSESSIONID"))
}


ShoppingBasketFacade.prototype.updateView = function() {
    this.vH.renderProducts(this.shoppingBasket);
    localStorage["shoppingbasket"] = JSON.stringify(this.shoppingBasket);
    this.removeProduct();
    this.updateProduct();
}
ShoppingBasketFacade.prototype.removeProduct = function() {
    this.vH.bindDelete((id) => {
        console.log(id);
        this.shoppingBasket.splice(id, 1);
        this.userDAO.updateShoppingBasket((data) => {}, { sessionid: document.cookie.split("=")[1], basket: this.shoppingBasket })
        this.updateView();
    });
}

ShoppingBasketFacade.prototype.updateProduct = function() {
    this.vH.bindUpdate((id, amount) => {
        this.shoppingBasket[id].amount = amount;
        this.userDAO.updateShoppingBasket((data) => {}, { sessionid: document.cookie.split("=")[1], basket: this.shoppingBasket })
        this.updateView();
    })
}

ShoppingBasketFacade.prototype.init = function() {
    this.vH.vhHeader.createHeader(false, false, false, false, true);
    this.loadShoppingBasket();
}

let f = new ShoppingBasketFacade();
f.init();