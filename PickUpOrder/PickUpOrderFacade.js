import { OrderDAO } from "../OrderDAO.js";
import { ViewHandlerPickUpOrder } from "./ViewHandlerPickUpOrder.js";

function PickUpOrderFacade() {
    this.orderDAO = new OrderDAO();
    this.vH = new ViewHandlerPickUpOrder();
}

PickUpOrderFacade.prototype.loadOrders = function() {
    this.orderDAO.loadOrders((orders) => {
        this.orders = JSON.parse(orders);
        this.vH.renderOrders(this.orders);
        this.changeStatusOfOrder();
    });
};

PickUpOrderFacade.prototype.changeStatusOfOrder = function() {
    this.vH.bindChangeStatus((id) => {
        this.orderDAO.changeStatus((result) => {
            this.loadOrders();
        }, id, { status: "Abgeholt" })
    })
}

PickUpOrderFacade.prototype.init = function() {
    this.loadOrders();
    this.vH.bindSearch();
};

let f = new PickUpOrderFacade();
f.init();