import { ge } from "../Helper/functions.js";
import { ViewHandlerFooter } from "../Helper/ViewHandlerFooter.js";

export function ViewHandlerPickUpOrder() {
    this.orders = document.querySelector(".orders");
    this.footer = new ViewHandlerFooter();
    this.search = ge("bestellNr");
}

ViewHandlerPickUpOrder.prototype.render = function(order) {
        let orderDate = order.orderdate.split("T");
        let deliveryDate = order.deliverydate.split("T");
        let tmp = document.createElement("template");
        let products = "",
            i;
        for (i = 0; i != order.products.length; i++) {
            products += order.products[i].name;
            if (i + 1 != order.products.length) {
                products += ", ";
            }
        }
        tmp.innerHTML = `<div  class="order list-group-item list-group-item-action flex-column align-items-start" data-id="${order._id}" pickup-nr="${order.pickUpNr}">
						<div id="dateAndBtn" class="d-flex w-100 justify-content-between">
						<headerOrder class="mb-1">Bestelldatum: <span>${orderDate[0]} ${orderDate[1]}</span>, Abholdatum: <span>${deliveryDate[0]} ${deliveryDate[1]}</span></headerOrder>
						<headerOrder class="mb-1">Abholnummer: <span> ${order.pickUpNr}</span></headerOrder>
						</div>
						<div class="d-flex w-100 justify-content-between">
						<p class="mb-">${products}</p> 
						<p class="sum" class="mb-2">€ ${order.sum}</p>
						</div>
						<div class="d-flex w-100 justify-content-between">
						<div class="status">Status: <span>${order.status ? order.status: " " }</span></div>
						${order.status!="Abgeholt" ? `						<div class="form-group">
						<div class="form-check">
						<span id="abgeholt">Abgeholt:</span>  
						<input id="checkBox"data-orderidforstatus="${order._id}"class="form-check-input" type="checkbox">
						  <label class="form-check-label" for="invalidCheck2">
						  </label>
						</div>
						</div>`: ``}
					</div>`;

    return tmp.content;
};

ViewHandlerPickUpOrder.prototype.bindChangeStatus = function(callback) {
    let checkBox = document.querySelectorAll("#checkBox");
    console.log(checkBox)
    for (let check of checkBox) {
        check.onclick = function() {
            callback(check.dataset.orderidforstatus);
            check.checked = true;
        };
    }
}

ViewHandlerPickUpOrder.prototype.renderOrders = function(orders) {
    this.orders = document.querySelector(".orders");
    let list = document.createElement("div");
    list.className = "list-group orders";
    list.id = "orders";
    let id = 0;
    if (orders != undefined) {
        orders
            .slice()
            .reverse()
            .forEach((order) => {
                console.log(order);
                let orderElem = this.render(order);
                list.append(orderElem);
                id++;
            });
    }
    console.log(this.orders);
    this.orders.replaceWith(list);
    this.orders = this.orders.list;
};

ViewHandlerPickUpOrder.prototype.searchOrder = function() {
    let filter, ul, li;
    filter = document.getElementById("bestellNr").value;
    ul = document.getElementById("orders");
    li = ul.getElementsByClassName("order list-group-item list-group-item-action flex-column align-items-start");

    for (let i = 0; i < li.length; i++) {
        let pickUpNr = li[i].getAttribute("pickup-nr");
        if (pickUpNr.indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
};

ViewHandlerPickUpOrder.prototype.bindSearch = function() {
    this.search.onchange = this.search.onkeyup = () => {
        this.searchOrder();
    };
};