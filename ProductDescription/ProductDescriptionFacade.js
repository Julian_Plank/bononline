"Use strict";

import { UserDAO } from "../UserDAO.js"
import { ViewHandlerProductDescription } from "./ViewHanlderProductDescription.js";
import { ProductDAO } from "../Products/ProductDAO.js";
import { readCookie, checkIfProductAlreadyExists, modifyAmount, validateAuth } from "../Helper/functions.js"

function ProductDescriptionFacade() {
    validateAuth();
    this.product;
    this.user;
    this.userDAO = new UserDAO();
    this.productDAO = new ProductDAO();
    this.vH = new ViewHandlerProductDescription();
}

ProductDescriptionFacade.prototype.loadProduct = function() {
    this.productDAO.loadProduct(product => {
        this.product = JSON.parse(product);
        this.vH.renderProduct(this.product)
        this.vH.bindBack();
        this.loadUser();
    }, JSON.parse(localStorage.getItem("descriptionId")))
}

ProductDescriptionFacade.prototype.loadUser = function() {
    this.userDAO.loadUser((data) => {
        this.user = JSON.parse(data);
        console.log(this.user)
        this.addProductToShoppingBasket()
        this.addProductToFavourites();
    }, readCookie("JSESSIONID"));
}

ProductDescriptionFacade.prototype.addProductToShoppingBasket = function() {
    this.vH.bindAddShoppingBasket(() => {
        let amount = Number(this.vH.getAmountOfProduct());
        this.product["amount"] = amount;
        if (checkIfProductAlreadyExists(this.product, this.user.shoppingbasket)) {
            modifyAmount(this.user.shoppingbasket, this.product)
        } else {
            this.user.shoppingbasket.push(this.product);
        }
        this.userDAO.updateShoppingBasket((data) => {}, {
            sessionid: document.cookie.split("=")[1],
            basket: this.user.shoppingbasket
        })

    })
}

ProductDescriptionFacade.prototype.addProductToFavourites = function() {
    this.vH.bindAddProductToFavourites(() => {
        console.log(this.user);
        if (checkIfProductAlreadyExists(this.product, this.user.favourites)) {
            this.vH.showModalForExistingFavourite();
        } else {

            this.user.favourites.push(this.product);
            this.userDAO.updateFavourites((data) => { console.log(data) }, {
                sessionid: document.cookie.split("=")[1],
                favourites: this.user.favourites
            })
        }
    })
}


ProductDescriptionFacade.prototype.init = function() {
    this.vH.vhHeader.createHeader(false, false, false, false, false);
    this.loadProduct();

}

let f = new ProductDescriptionFacade();
f.init();