"use strict";

import { ge } from "../Helper/functions.js";
import { ViewHandlerHeader } from "../Helper/ViewHandlerHeader.js";
import { Modal } from "../Helper/Modal.js";
import { ViewHandlerFooter } from "../Helper/ViewHandlerFooter.js";

export function ViewHandlerProductDescription() {
	this.descriptionElement = ge("beschreibung");
	this.vhHeader = new ViewHandlerHeader();
	this.footer = new ViewHandlerFooter();
	this.modal = new Modal();
}

ViewHandlerProductDescription.prototype.createContainer = function (className) {
	let container = document.createElement("div");
	container.className = className;
	return container;
};

ViewHandlerProductDescription.prototype.createBigButton = function (className, id, innerHtml, typeOfButton) {
	let div = document.createElement("div");
	div.className = className;
	let btn = document.createElement("button");
	btn.id = id;
	btn.className = id + " btn btn-" + typeOfButton;
	btn.innerHTML = innerHtml;
	div.append(btn);

	return div;
};

ViewHandlerProductDescription.prototype.getAmountOfProduct = function () {
	return ge("amount").value;
};

ViewHandlerProductDescription.prototype.bindAddShoppingBasket = function (callback) {
	let addBtn = ge("addToShoppingBasket");
	addBtn.onclick = () => {
		this.modal.modifyBody("Produkt wurde zum Warenkorb hinzugefügt! Zum Warenkorb gehen?");
		this.modal.container.querySelector("#yes").onclick = function () {
			window.location.href = "/ShoppingBasket/ShoppingBasket.html";
		};
		callback();
		jQuery(this.modal.container).modal("show");
	};
};

ViewHandlerProductDescription.prototype.bindBack = function () {
	document.getElementById("back").onclick = function () {
		window.location.href = "/Products/Products.html";
	};
};

ViewHandlerProductDescription.prototype.bindAddProductToFavourites = function (callback) {
	document.getElementById("favourites").onclick = () => {
		this.modal.modifyBody("Produkt wurde zu Favoriten hinzugefügt! Gehe zu Favoriten?");
		this.modal.container.querySelector("#yes").onclick = function () {
			window.location.href = "/shop/Shop.html";
		};
		callback();
		jQuery(this.modal.container).modal("show");
	};
};

ViewHandlerProductDescription.prototype.showModalForExistingFavourite = function () {
	this.modal.modifyBody("Produkt ist bereits ein Favorit! Gehe zu Favoriten?");
	this.modal.container.querySelector("#yes").onclick = function () {
		window.location.href = "/shop/Shop.html";
	};
	jQuery(this.modal.container).modal("show");
};

ViewHandlerProductDescription.prototype.createTitle = function (title) {
	let div = document.createElement("div");
	div.className = "titleProduct";
	div.innerHTML = title;

	return div;
};

ViewHandlerProductDescription.prototype.createImage = function (imgName) {
	let div = document.createElement("div");
	div.className = "imgOfProduct";
	let img = document.createElement("img");
	img.id = "product";
	img.src = `../Assets/Images/${imgName.replace(" ", "_")}.jpg`;
	img.alt = imgName;
	div.append(img);
	return div;
};

ViewHandlerProductDescription.prototype.createFixedDescription = function (info) {
	let div = document.createElement("div");
	div.className = "fixed";
	div.innerHTML = info;
	return div;
};

ViewHandlerProductDescription.prototype.createIndividualDescription = function (info, productDescription) {
	let div = document.createElement("div");
	div.className = "individual";
	div.innerHTML = productDescription;
	return div;
};

ViewHandlerProductDescription.prototype.createInputForAmount = function () {
	let div = document.createElement("div");
	div.innerHTML = `<input type="number" min="1" value="1" class="form-control" id="amount">`;
	return div;
};

ViewHandlerProductDescription.prototype.createButtonsForDescriptionAndFavorites = function () {
	let div = document.createElement("div");
	div.className = "buttons";
	let btn1 = document.createElement("button");
	btn1.id = "description";
	let btn2 = document.createElement("button");
	btn2.id = "favourites";
	btn1.className = "btn btn-outline-dark btn-lg";
	btn2.className = "btn btn-outline-dark btn-lg";
	btn1.innerHTML = '<i class="fa-solid fa-info"></i>';
	btn2.innerHTML = '<i class="fa-solid fa-star"></i>';
	div.append(btn1);
	div.append(btn2);
	return div;
};

ViewHandlerProductDescription.prototype.createFirstContainer = function (product) {
	let container = this.createContainer("container");
	container.append(this.createBigButton("backBtnDiv", "back", `  <i class="fa-solid fa-arrow-left"></i>`, "secondary"));
	container.append(this.createTitle(product.name));
	container.append(this.createImage(product.name));

	return container;
};

ViewHandlerProductDescription.prototype.createSecondContainer = function (product) {
	let container2 = this.createContainer("container2");
	container2.append(this.createFixedDescription("Preis:"));
	container2.append(this.createIndividualDescription("Preis", product.price));
	container2.append(this.createFixedDescription("Anzahl:"));
	container2.append(this.createInputForAmount());
	container2.append(this.createFixedDescription("Beschreibung:"));
	container2.append(this.createIndividualDescription("Beschreibung", product.description));
	container2.append(this.createButtonsForDescriptionAndFavorites());
	container2.append(this.createBigButton("zumWarenkorbDiv", "addToShoppingBasket", `<i class="fa-solid fa-cart-plus"></i>`, "success"));

	return container2;
};

ViewHandlerProductDescription.prototype.renderProduct = function (product) {
	let container = this.createFirstContainer(product);
	let container2 = this.createSecondContainer(product);
	container.append(container2);
	this.descriptionElement.append(container);
};
