import modal from "../Templates/Modal.template.js"
import { createElementFromHtml } from "./DomUtils.js";

export function Modal() {
    this.container = createElementFromHtml(modal());
}

Modal.prototype.modifyTitle = function(title) {
    this.container.querySelector('.modal-title').innerHTML = title;
}

Modal.prototype.modifyBody = function(body) {
    this.container.querySelector('.modal-body').innerHTML = body;
}

Modal.prototype.modifyFooter = function(button) {
    this.container.querySelector('.modal-footer').append(button);
}