import { ge } from "./functions.js";
import { createElementFromHtml } from "../Helper/DomUtils.js";
import footer from "../Templates/Footer.template.js";

export function ViewHandlerFooter() {
	this.footer = ge("footer").append(createElementFromHtml(footer()));
}
