"use strict";

const baseurl = "http://localhost:3000";

function ge(id) {
	return document.getElementById(id);
}

function readCookie(name) {
	const cookieArr = document.cookie.split(";");

	// Loop through the array elements
	for (let i = 0; i < cookieArr.length; i++) {
		var cookiePair = cookieArr[i].split("=");

		/* Removing whitespace at the beginning of the cookie name
        and compare it with the given string */
		if (name == cookiePair[0].trim()) {
			// Decode the cookie value and return
			return decodeURIComponent(cookiePair[1]);
		}
	}
}

/*
Anmerkung zu den Änderungen in validateAuth:
Wenn bei einem Cookie das Attribut "Expires" existiert, löscht der Browser das
Cookie automatisch, sobald eine gewisse Zeit erreicht ist. Deswegen ist beim Server
kein Login-Check notwendig, da der Client direkt erkennen kann ob das Cookie noch gültig ist
oder nicht (ist es nicht gültig, steht nur mehr ein leerer String in document.cookie, da der 
Browser es dann schon gelöscht hat). Vorher hat das übrigens nicht funktioniert, da die entsprechenden
Änderungen bei der Webuntis-Schnittstelle noch notwendig waren. Das Cookie hat standardmäßig eine Gültigkeit
von 10 Minuten, das kann aber beliebig verändert werden.

Thomas
*/
function validateAuth(loginPage = false) {
	/*
	const xhr = new XMLHttpRequest();

	xhr.open("POST", `${baseurl}/login/check`);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send(
		JSON.stringify({
			sessionid: document.cookie.split("=")[1],
		})
	);

	xhr.onload = function () {
		if (xhr.status == 200) {
			if (!loginPage && xhr.response == "false") {
				redirectToLogin();
			} else if (loginPage && xhr.response == "true") {
				redirectToPath("shop/Shop.html");
			}
		} else {
			redirectToLogin();
		}
	};
	*/
	if (!loginPage && !document.cookie) {
		redirectToLogin();
	} else if (loginPage && document.cookie) {
		redirectToPath("shop/Shop.html");
	}
}

function logOut() {
	const xhr = new XMLHttpRequest();

	let sessionid = document.cookie.split("=")[1];

	xhr.open("GET", `${baseurl}/logout/${sessionid}`);

	xhr.send();

	xhr.onload = function () {
		if (xhr.status == 200) {
			document.cookie = xhr.responseText;
			redirectToLogin();
		}
	};
}

function redirectToLogin() {
	redirectToPath("login/Login.html");
}

function redirectToPath(path) {
	window.location.replace("http://127.0.0.1:5501/" + path);
}

function calculateTotal(items) {
	let total = 0;
	for (let item in items) {
		total += item.price;
	}
	return total;
}

function modifyAmount(list, product) {
	for (let p of list) {
		if (p._id == product._id) {
			p.amount += product.amount;
		}
	}
}

function checkIfProductAlreadyExists(product, list) {
	for (let p of list) {
		if (p._id == product._id) {
			return true;
		}
	}
	return false;
}

function checkIfMenuAlreadyExists(menu, list) {
	for (let p of list) {
		if (p.date == menu.date && p.category == menu.category) {
			return true;
		}
	}
	return false;
}

function modifyAmountMenu(list, menu) {
	for (let p of list) {
		if (p.date == menu.date && p.category == menu.category) {
			p.amount += menu.amount;
		}
	}
}

function getWeek(dt) {
	let tdt = new Date(dt.valueOf());
	let dayn = (dt.getDay() + 6) % 7;
	tdt.setDate(tdt.getDate() - dayn + 3);
	let firstThursday = tdt.valueOf();
	tdt.setMonth(0, 1);
	if (tdt.getDay() !== 4) {
		tdt.setMonth(0, 1 + ((4 - tdt.getDay() + 7) % 7));
	}
	return 1 + Math.ceil((firstThursday - tdt) / 604800000);
}

function getCurrentTime() {
	let current = new Date();
	return (
		current.getFullYear() +
		"-" +
		(current.getMonth() + 1 < 10 ? "0" : "") +
		(current.getMonth() + 1) +
		"-" +
		(current.getDate() < 10 ? "0" : "") +
		current.getDate() +
		"T" +
		(current.getHours() < 10 ? "0" : "") +
		current.getHours() +
		":" +
		(current.getMinutes() < 10 ? "0" : "") +
		current.getMinutes()
	);
}

function getDayDifference(datee1, datee2) {
	const date1 = new Date(datee1);
	const date2 = new Date(datee2);

	//return parseInt((date2 - date1) / (1000 * 60 * 60 * 24), 10);

	return parseInt(date2.getDate() - date1.getDate());
}

function getMinuteDifference(datee1, datee2) {
	const date1 = new Date(datee1);
	const date2 = new Date(datee2);
	/* let diff = (date1.getTime() - date2.getTime()) / 1000;
	diff /= 60; */
	let minutes = parseInt((Math.abs(date1.getTime() - date2.getTime()) / (1000 * 60)) % 60);
	if (date1.getTime() < date2.getTime()) {
		return -Math.abs(minutes);
	} else {
		return minutes;
	}
}

export {
	ge,
	modifyAmountMenu,
	readCookie,
	modifyAmount,
	checkIfProductAlreadyExists,
	calculateTotal,
	getWeek,
	validateAuth,
	redirectToLogin,
	logOut,
	redirectToPath,
	checkIfMenuAlreadyExists,
	getCurrentTime,
	getDayDifference,
	getMinuteDifference,
};
