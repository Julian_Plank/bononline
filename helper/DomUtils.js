export function createElementsFromHtml(html) {
	let elem = document.createElement("template");
	elem.innerHTML = html;
	return elem.content.childNodes;
}
export function createElementFromHtml(html) {
	let elem = document.createElement("template");
	elem.innerHTML = html;
	return elem.content.firstChild;
}
