"use strict";

import { ge } from "./functions.js";

export function ViewHandlerHeader() {
	this.header = ge("header");
}

ViewHandlerHeader.prototype.createHeader = function (active1, active2, actice3, active4, active5) {
	this.header.append(this.createUl(active1, active2, actice3, active4, active5));
};

ViewHandlerHeader.prototype.createUl = function (active1, active2, actice3, active4, active5) {
	let ul = document.createElement("ul");
	ul.append(this.createLi(false, "/shop/Shop.html", `<img src="../Helper/logo_small.png" height="24px" width="80px"/>`));
	ul.append(this.createLi(active1, "/shop/Shop.html", "Startseite"));
	ul.append(this.createLi(active2, "/Products/Products.html", "Produkte"));
	ul.append(this.createLi(actice3, "/WeekMenu/WeekMenu.html", "Wochenmenü"));
	ul.append(this.createLiIcons(active4, "/Profile/Profile.html", `<i class="fa-solid fa-user"<i>`));
	ul.append(this.createLiIcons(active5, "/ShoppingBasket/ShoppingBasket.html", `<i class="fas fa-shopping-cart"<i>`));
	return ul;
};

ViewHandlerHeader.prototype.createLi = function (active, href, innerHtml) {
	let li = document.createElement("li");
	let a = document.createElement("a");
	if (active) {
		a.classList.add("active");
	}
	a.href = href;
	a.innerHTML = innerHtml;
	li.append(a);
	return li;
};

ViewHandlerHeader.prototype.createLiIcons = function (active, href, iClass) {
	let li = document.createElement("li");
	li.style.float = "right";
	let a = document.createElement("a");
	if (active) {
		a.classList.add("active");
	}
	a.href = href;
	a.innerHTML = iClass;
	a.classList.add("icons");
	li.append(a);
	return li;
};
