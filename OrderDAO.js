import { BaseDAO } from "./BaseDAO.js";

export function OrderDAO() {
    this.baseDAO = new BaseDAO();
}

OrderDAO.prototype.loadOrders = function(callback) {
    let xhr = this.baseDAO.prepareRequest("GET", `/orders`, {});
    xhr.onload = () => callback(xhr.response);
    xhr.send();
}

OrderDAO.prototype.loadOrdersFromUser = function(callback, customerNr) {
    let xhr = this.baseDAO.prepareRequest("GET", `/orders/${customerNr}`, {});
    xhr.onload = () => callback(xhr.response);
    xhr.send();
}

OrderDAO.prototype.deleteOrder = function(callback, customerNr) {
    let xhr = this.baseDAO.prepareRequest("DELETE", `/orders/${customerNr}`, {});
    xhr.onload = () => callback(xhr.response);
    xhr.send();
}

OrderDAO.prototype.changeStatus = function(callback, id, data) {
    let xhr = this.baseDAO.prepareRequest("PUT", `/orders/${id}`, {});
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = () => callback(xhr.response);
    console.log(data, id);
    xhr.send(JSON.stringify(data));
}