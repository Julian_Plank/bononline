export function Order(date, sum, products, cancelable) {
	this.date = date;
	this.sum = sum;
	this.products = products;
	this.cancelable = cancelable;
}
