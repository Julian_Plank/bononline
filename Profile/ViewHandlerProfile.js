"use strict";

import { ge } from "/Helper/functions.js";
import { ViewHandlerHeader } from "../Helper/ViewHandlerHeader.js";
import { logOut, getCurrentTime, getDayDifference, getMinuteDifference } from "../Helper/functions.js";
import { Modal } from "../Helper/Modal.js";
import { ViewHandlerFooter } from "../Helper/ViewHandlerFooter.js";

export function ViewHandlerProfile() {
	this.header = ge("header");
	this.vhHeader = new ViewHandlerHeader();
	this.footer = new ViewHandlerFooter();
	this.orders = document.querySelector(".orders");
	this.modal = new Modal();
}

ViewHandlerProfile.prototype.showBalance = function (balance) {
	let balance2 = String(balance).split(".");
	let comma;
	balance2[1] != undefined ? (comma = "0€") : (comma = "00€");
	let endBalance;
	if (balance2[1] == undefined) {
		endBalance = balance2[0] + "," + comma;
	} else {
		endBalance = balance2[0] + "," + balance2[1] + comma;
	}

	let tmp = document.createElement("form");
	tmp.innerHTML = `<div class="balance form-group row">
    <label for="balance" class="col-md-2 col-form-label col-form-label-lg"><h3>Guthaben:</h3></label>
    <div class="col-lg-1">
      <input type="output" class="form-control form-control-lg" id="balance" placeholder="${endBalance}" readonly>
    </div>
  </div>`;
	document.querySelector(".balance").replaceWith(tmp);
};

ViewHandlerProfile.prototype.bindDelete = function (callback) {
	let orders = Array.from(document.querySelectorAll(".order"));

	for (let order of orders) {
		console.log(order);
		order.querySelector("#delBtn").onclick = () => {
			this.modal.modifyBody("Möchte Sie wirklich Ihre Bestellung stornieren? Ihre Ausgaben werden Ihrem Guthaben gutgeschrieben!");
			this.modal.container.querySelector("#yes").onclick = () => {
				callback(order.dataset.id);
				jQuery(this.modal.container).modal("hide");
			};
			jQuery(this.modal.container).modal("show");
		};
	}
};

ViewHandlerProfile.prototype.showNameAndLogout = function (name) {
	let tmp = document.createElement("form-inline");
	tmp.innerHTML = `<div class="form-group row">
    <label for="user" class="col-sm-2 col-form-label col-form-label-lg"><h3>Nutzername: </h3></label>
    <div class="col-lg-2">
      <input type="output" class="form-control form-control-lg" id="user" placeholder="${name}" readonly>
    </div>
    <div class="col-lg-1">
    <button id="signoutBtn" class="btn btn-danger btn-lg"><i class="fa fa-sign-out"></i></button>
    </div>
    
</div>`;
	tmp.querySelector("#signoutBtn").onclick = logOut;
	document.querySelector(".name").append(tmp);
};

ViewHandlerProfile.prototype.render = function (order) {
	let orderDate = order.orderdate.split("T");
	let deliveryDate = order.deliverydate.split("T");
	let tmp = document.createElement("template");
	let products = "",
		i;
	for (i = 0; i != order.products.length; i++) {
		products += order.products[i].name;
		if (i + 1 != order.products.length) {
			products += ", ";
		}
	}
	if (this.checkForCancellation(getCurrentTime(), order.deliverydate)) {
		tmp.innerHTML = `<div  class="order list-group-item list-group-item-action flex-column align-items-start" data-id="${order._id}">
						<div id="dateAndBtn" class="d-flex w-100 justify-content-between">
						<headerOrder class="mb-1">Bestelldatum: <span>${orderDate[0]} ${orderDate[1]}</span>, Abholdatum: <span>${deliveryDate[0]} ${deliveryDate[1]}</span></headerOrder>
						<button id="delBtn"class="btn btn-danger"><i class="fa fa-times"></i></button>
						</div>
						<div class="d-flex w-100 justify-content-between">
						<p class="mb-">${products}</p> 
						<p class="sum" class="mb-2">€ ${order.sum}</p>
						</div>
					</div>`;
	} else {
		tmp.innerHTML = `<div  class="order list-group-item list-group-item-action flex-column align-items-start" data-id="${order._id}">
						<div id="dateAndBtn" class="d-flex w-100 justify-content-between">
						<headerOrder class="mb-1">Bestelldatum: <span>${orderDate[0]} ${orderDate[1]}</span>, Abholdatum: <span>${deliveryDate[0]} ${deliveryDate[1]}</span></headerOrder>
                        </div>
						<div class="d-flex w-100 justify-content-between">
						<p class="mb-">${products}</p> 
						<p class="sum" class="mb-2">€ ${order.sum}</p>
						</div>
					</div>`;
	}

	return tmp.content;
};

ViewHandlerProfile.prototype.checkForCancellation = function (current, delivery) {
	if (getDayDifference(current, delivery) < 0) {
		return false;
	} else if (getDayDifference(current, delivery) > 0) {
		return true;
	} else if (getDayDifference(current, delivery) == 0) {
		if (getMinuteDifference(current, delivery) > -30) {
			return false;
		} else if (getMinuteDifference(current, delivery) <= -30) {
			return true;
		}
		return true;
	}
};

ViewHandlerProfile.prototype.renderOrders = function (orders) {
	this.orders = document.querySelector(".orders");
	let list = document.createElement("div");
	list.className = "list-group orders";
	list.id = "orders";
	let id = 0;
	if (orders != undefined) {
		orders
			.slice()
			.reverse()
			.forEach((order) => {
				console.log(order);
				let orderElem = this.render(order);
				list.append(orderElem);
				id++;
			});
	}
	console.log(this.orders);
	this.orders.replaceWith(list);
	this.orders = this.orders.list;
};
