import { ViewHandlerProfile } from "./ViewHandlerProfile.js";
import { UserDAO } from "../UserDAO.js";
import { OrderDAO } from "../OrderDAO.js";
import { readCookie, validateAuth } from "../helper/functions.js";
import { OrderMocks } from "../Mock/OrderMocks.js";

function ProfilFacade() {
    validateAuth();
    this.vH = new ViewHandlerProfile();
    this.userDAO = new UserDAO();
    this.orderDAO = new OrderDAO();
    this.user = undefined;
    this.orders = [];
}

ProfilFacade.prototype.updateView = function() {
    this.loadOrders();
    this.loadUserNameAndLogout();
    this.loadBalance();
};

ProfilFacade.prototype.loadUserData = function() {
    this.userDAO.loadUser((user) => {
        this.user = JSON.parse(user);
        this.updateView();
    }, readCookie("JSESSIONID"));
};

ProfilFacade.prototype.loadUserNameAndLogout = function() {
    this.vH.showNameAndLogout(this.user.username);
};

ProfilFacade.prototype.loadBalance = function() {
    this.vH.showBalance(this.user.balance);
};

ProfilFacade.prototype.bindDeleteOrder = function() {
    this.vH.bindDelete((id) => {
        for (let order of this.orders) {
            if (order._id == id) {
                this.user.balance += order.sum;

                this.userDAO.updateBalance(() => {}, {
                    sessionid: document.cookie.split("=")[1],
                    balance: this.user.balance
                })
                this.loadBalance();
            }
        }
        this.orderDAO.deleteOrder((order) => {
            this.loadOrders();
        }, id)
    })
}


ProfilFacade.prototype.loadOrders = function() {
    this.orderDAO.loadOrdersFromUser((orders) => {
        this.orders = JSON.parse(orders);
        this.vH.renderOrders(this.orders);
        this.bindDeleteOrder();
        //this.vH.renderOrders(new OrderMocks().list);
    }, this.user._id)
};

ProfilFacade.prototype.init = function() {
    this.vH.vhHeader.createHeader(false, false, false, true, false);
    this.loadUserData();
};

let f = new ProfilFacade();
f.init();