import { redirectToPath, validateAuth, readCookie } from "../helper/functions.js";
import { UserDAO } from "../UserDAO.js"

let orderedProducts = [];
let orderedProductsForStoring = [];

let deliverydate = "";
let totalSum;
let totalAmount = 0;
let totalTax = 0;
let balance = 0;


validateAuth();
setTotalSum();
loadBalance();

paypal.Button.render({
        //Einstellungen Konto
        env: "sandbox",
        client: {
            sandbox: "ATVF8pzMnqc_kMbgaDULcPSbRBIbNz8p2mZNq1HAdpumlsuvMv80y_1r4LlqKjV-G7Etp6QI1VLgYZR0",
            //production: 'demo_production_client_id'
        },

        //Paypal Button
        locale: "de_AT",
        style: {
            size: "small",
            color: "blue",
            shape: "pill",
        },

        //PayNow Checkout true/false
        commit: true,

        payment: function(data, actions) {
            let diff = totalAmount - totalSum;
            let paypalTotalAmount = Number(totalSum);
            orderedProducts.push(new PayPalProduct("Guthaben", "Abzug von Guthaben", "1", String(-diff), "0", "EUR"));
            return actions.payment.create({
                transactions: [{
                    amount: {
                        total: String(paypalTotalAmount.toFixed(2)),
                        currency: "EUR",
                        details: {
                            subtotal: String((paypalTotalAmount - totalTax).toFixed(2)),
                            tax: String(totalTax.toFixed(2)),
                        },
                    },
                    description: "Zahlung für Buffet-Kauf.",
                    custom: "90048630024435",
                    payment_options: {
                        allowed_payment_method: "INSTANT_FUNDING_SOURCE",
                    },
                    soft_descriptor: "Beschreibung",
                    item_list: {
                        items: orderedProducts,
                    },
                }, ],
                note_to_payer: "Mögliche Notiz",
            });
        },

        // Zahlung ausführen
        onAuthorize: function(data, actions) {
            return actions.payment.execute().then(function() {
                // Add order and send email
                removeBalanceAndAddOrder();
            });
        },
        onApprove: function(data, actions) {
            return actions.order.capture().then(function(orderData) {
                // Successful capture! For dev/demo purposes:
                console.log("Capture result", orderData, JSON.stringify(orderData, null, 2));
                let transaction = orderData.purchase_units[0].payments.captures[0];
                alert("Transaction " + transaction.status + ": " + transaction.id + "\n\nSee console for all available details");
            });
        },
    },
    "#paypal"
);

document.querySelector("#ordering").onclick = removeBalanceAndAddOrder;

const openTime = "07:00";
const closeTime = "17:00";

const menuOpenTime = "11:00";
const menuCloseTime = "14:00";

let today = formatDate(new Date());
let timepicker = document.getElementById("deliverytime");
timepicker.setAttribute("min", today);

timepicker.onchange = function() {
    let date = new Date();
    date.setMinutes(date.getMinutes() + 30);
    let currentTime = dateToTime(date);
    let pickedDate = timepicker.value.split("-");
    let pickedTime = timepicker.value.split("T")[1];
    if (pickedTime < openTime || pickedTime > closeTime) {
        validateResult(`Bitte gib ein Zeitpunkt zwischen ${openTime} und ${closeTime} ein!`);
        return;
    }
    if (new Date(timepicker.value).getDay() == 0 || new Date(timepicker.value).getDay() == 6) {
        validateResult("Bitte gib ein Zeitpunkt von Montag bis Freitag ein!");
        return;
    }
    if (pickedTime < currentTime && !checkDay(pickedDate[0], pickedDate[1], pickedDate[2].split("T")[0])) {
        validateResult(`Der eingegebene Zeitpunkt muss 30 Minuten in der Zukunft liegen!`);
        return;
    }
    if (!checkMenuTime(pickedTime, pickedDate)) {
        validateResult(`Bestellungen mit Menüs müssen im Zeitraum von 11:00 bis 14:00 abgeholt werden!`);
        return;
    }
    if (new Date(timepicker.value).getDay() == 0 || new Date(timepicker.value).getDay() == 6) {
        validateResult("Bitte gib ein Zeitpunkt von Montag bis Freitag ein!");
        return;
    }
    if (pickedTime < currentTime && !checkDay(pickedDate[0], pickedDate[1], pickedDate[2].split("T")[0])) {
        validateResult(`Der eingegebene Zeitpunkt muss 30 Minuten in der Zukunft liegen!`);
        return;
    }
    console.log(checkMenuTime(pickedTime, pickedDate));
    if (!checkMenuTime(pickedTime, pickedDate)) {
        validateResult(`Bestellungen mit Menüs müssen im Zeitraum von 11:00 bis 14:00 abgeholt werden!`);
        return;
    }
    validateResult("");
    deliverydate = timepicker.value;
};

function checkMenuTime(time, date) {
    let result = true;
    if (localStorage["shoppingbasket"] != undefined) {
        let basket = JSON.parse(localStorage["shoppingbasket"]);
        basket.forEach((element) => {
            if (element.type == "menu") {
                let menuDate = element.date.split(".");
                if (
                    time <= menuCloseTime &&
                    time >= menuOpenTime &&
                    menuDate[0] == date[2].split("T")[0] &&
                    menuDate[1] == date[1] &&
                    menuDate[2] == date[0]
                ) {
                    return;
                }
                result = false;
            }
        });
    }
    return result;
}

function addOrder() {
    let xhr = new XMLHttpRequest();

    xhr.open("POST", "http://localhost:3000/orders/", true);

    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onload = function() {
        if (xhr.status == 200) {
            resetBasket();
        } else {
            console.log("Add order Failed");
        }
    };
    console.log(orderedProductsForStoring);
    xhr.send(JSON.stringify(new Order(deliverydate, totalAmount, formatDate(new Date()), orderedProductsForStoring, document.cookie.split("=")[1])));
}

function removeBalanceAndAddOrder() {
    let xhr = new XMLHttpRequest();

    xhr.open("PUT", "http://localhost:3000/users/balance", true);

    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onload = function() {
        if (xhr.status == 200) {
            addOrder();
        } else {
            console.log("Remove Balance Failed");
        }
    };
    balance -= totalAmount;
    if (balance < 0) {
        balance = 0;
    }
    xhr.send(JSON.stringify({ sessionid: document.cookie.split("=")[1], balance: balance }));
}

function resetBasket() {
    let xhr = new XMLHttpRequest();

    xhr.open("PUT", "http://localhost:3000/users/basket", true);

    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onload = function() {
        if (xhr.status == 200) {
            redirectToPath("ShoppingBasket/ShoppingBasket.html");
        } else {
            console.log("Reset Basket Failed");
        }
    };
    xhr.send(JSON.stringify({ sessionid: document.cookie.split("=")[1], basket: [] }));
}

function checkDay(year, month, day) {
    let currentDate = new Date();
    if (year < currentDate.getFullYear()) {
        return false;
    }
    if (year == currentDate.getFullYear()) {
        if (month < currentDate.getMonth() + 1) {
            return false;
        }
        if (month == currentDate.getMonth() + 1) {
            if (day <= currentDate.getDate()) {
                return false;
            }
        }
    }
    return true;
}

function formatDate(date) {
    let dd = date.getDate();
    let mm = date.getMonth() + 1;
    let yyyy = date.getFullYear();
    let hour = date.getHours();
    let minute = date.getMinutes();

    if (dd < 10) {
        dd = "0" + dd;
    }
    if (mm < 10) {
        mm = "0" + mm;
    }
    if (hour < 10) {
        hour = "0" + hour;
    }
    if (minute < 10) {
        minute = "0" + minute;
    }
    return yyyy + "-" + mm + "-" + dd + "T" + dateToTime(date);
}

function validateResult(result) {
    document.getElementById("errorname").innerHTML = result;
    let paypal = document.getElementById("paypal");
    let orderButton = document.getElementById("ordering");
    if (result == "") {
        if (totalSum == 0) {
            orderButton.hidden = false;
        } else {
            paypal.hidden = false;
        }
    } else {
        paypal.hidden = true;
        orderButton.hidden = true;
    }
}

function dateToTime(date) {
    let hour = date.getHours();
    let minute = date.getMinutes();
    if (hour < 10) {
        hour = "0" + hour;
    }
    if (minute < 10) {
        minute = "0" + minute;
    }
    return hour + ":" + minute;
}

function createUUID() {
    let s = [];
    let hexDigits = "0123456789abcdef";
    for (let i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
    s[8] = s[13] = s[18] = s[23] = "-";
    return s.join("");
}

function Order(deliverydate, sum, orderdate, products, sessionid) {
    this.sum = sum;
    this.deliverydate = deliverydate;
    this.orderdate = orderdate;
    this.products = products;
    this.sessionid = sessionid;
    this.status = "Offen";
}

function PayPalProduct(name, description, quantity, price, tax, currency) {
    this.name = name;
    this.description = description;
    this.quantity = quantity;
    this.price = price;
    this.tax = tax;
    this.currency = currency;
}

function ProductForStoring(name, description, quantity, price, tax, currency, type, id) {
    this.name = name;
    this.description = description;
    this.quantity = quantity;
    this.price = price;
    this.tax = tax;
    this.currency = currency;
    this.type = type;
    this._id = id;
}

function setTotalSum() {
    totalAmount = 0;
    totalTax = 0;
    orderedProducts = [];
    orderedProductsForStoring = [];
    if (localStorage["shoppingbasket"] != undefined) {
        let basket = JSON.parse(localStorage["shoppingbasket"]);
        basket.forEach((element) => {
            if (element.price.includes(",")) {
                element.price = Number(element.price.split(",")[0]) + Number(element.price.split(",")[1].substring(0, 2)) / 100;
            } else {
                element.price = Number(element.price.split("€")[0]);
            }
            let tempTax = (Math.round((element.price / 6) * 100, 2) / 100).toFixed(2);
            let type;
            element.type == "menu" ? (type = element.type + element.category) : (type = "product");
            orderedProductsForStoring.push(
                new ProductForStoring(
                    element.name,
                    element.description,
                    String(element.amount),
                    String(element.price.toFixed(2) - tempTax),
                    String(tempTax),
                    "EUR",
                    type,
                    element._id,
                )
            );
            orderedProducts.push(
                new PayPalProduct(
                    element.name,
                    element.description,
                    String(element.amount),
                    String(element.price.toFixed(2) - tempTax),
                    String(tempTax),
                    "EUR"
                )
            );
            totalAmount += element.price * element.amount;
            totalTax += (Math.round((element.price / 6) * 100, 2) * element.amount) / 100;
        });
        let total = totalAmount.toFixed(2) + "€";
        total = total.replace(".", ",");
        document.querySelector("#totalSumInput").placeholder = total;
    }
}

function loadBalance() {
    let userDAO = new UserDAO();
    let user;
    userDAO.loadUser((user2) => {
        user = JSON.parse(user2);
        balance = user.balance;
        setBalance(balance);
        setTotalSumAfterBalance();
    }, readCookie("JSESSIONID"));
}

function setBalance(balance) {
    let balanceWithSplit = String(balance).split(".");
    let comma, endBalance;
    balanceWithSplit[1] == undefined ? (comma = "00€") : (comma = "0€");
    if (balanceWithSplit[1] == undefined) {
        endBalance = balanceWithSplit[0] + "," + comma;
    } else {
        endBalance = balanceWithSplit[0] + "," + balanceWithSplit[1] + comma;
    }
    document.querySelector("#balance").placeholder = endBalance;
}

function setTotalSumAfterBalance() {
    let totalSum2 = document.querySelector("#totalSumInput").placeholder;
    let balance = document.querySelector("#balance").placeholder;
    let endSum = stringToNumber(totalSum2) - stringToNumber(balance);
    if (endSum <= 0) {
        totalSum = 0;
    } else {
        totalSum = endSum.toFixed(2);
    }
    document.querySelector("#totalSumOutputWithBalance > h2").innerHTML = String(totalSum).replace(".", ",") + "€";
}

function stringToNumber(sum) {
    let d = sum.split(",");
    let afterComma = d[1].split("€");
    let totalSum = Number(d[0] + "." + afterComma[0]);
    return totalSum;
}