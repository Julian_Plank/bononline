"use strict";

import { ViewHandlerHeader } from "../Helper/ViewHandlerHeader.js";
import { Modal } from "../Helper/Modal.js";
import { ViewHandlerFooter } from "../Helper/ViewHandlerFooter.js";


export function ViewHandlerImprint() {
	this.vhHeader = new ViewHandlerHeader();
	this.footer = new ViewHandlerFooter();
	this.modal = new Modal();
}

ViewHandlerImprint.prototype.init = function() {
    this.vhHeader.createHeader(false, false, false, false, false);
}

let viewHandlerImprint = new ViewHandlerImprint().init();
