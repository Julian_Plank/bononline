import { Product } from "../Products/Product.js";

export function ProductMocks() {
	this.list = new Array(
		new Product("Coca-Cola", "-", "€ 2,50", false),
		new Product("Wurstsemmel", "-", "€ 2,00", false),
		new Product("Jolly", "-", "€ 1,50", false),
		new Product("Kornspitz", "-", "€ 3,00", false)
	);
}
