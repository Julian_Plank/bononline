import { Order } from "../Profile/Order.js";
import { Product } from "../Products/Product.js";

export function OrderMocks() {
	this.list = new Array(
		new Order(
			"14.04.2022",
			"€ 6,00",
			new Array(
				new Product("Coca-Cola", "-", "€ 2,50", false),
				new Product("Wurstsemmel", "-", "€ 2,00", false),
				new Product("Jolly", "-", "€ 1,50", false)
			),
			true
		),
		new Order(
			"01.04.2022",
			"€ 18,50",
			new Array(
				new Product("Käsebrot", "-", "€ 2,00", false),
				new Product("Wurstsemmel", "-", "€ 2,00", false),
				new Product("Kornspitz", "-", "€ 3,00", false),
				new Product("Curyl Fries", "-", "€ 3,50", false),
				new Product("Brezensnack", "-", "€ 2,50", false)
			),
			true
		),
		new Order(
			"25.03.2022",
			"€ 6,00",
			new Array(
				new Product("Coca-Cola", "-", "€ 2,50", false),
				new Product("Wurstsemmel", "-", "€ 2,00", false),
				new Product("Jolly", "-", "€ 1,50", false)
			),
			true
		)
	);
}
