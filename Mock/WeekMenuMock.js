import { Day } from "../WeekMenu/Day.js";
import { WeekMenu } from "../WeekMenu/WeekMenu.js";

export function WeekMenuMock() {
    this.weekMenu = new WeekMenu(
        new Array(
            new Day("04.04.2022", "Walliser Schnitzel mit Reis und Salat", "Gerösteter Knödel mit Ei und Salat"),
            new Day("05.04.2022", "Cordon-bleu mit Petersilienkartoffeln und Salat", "Mohnnudeln mit Fruchtsalat"),
            new Day("06.04.2022", "Gegrilltes Hühnerfilet mit Gemüse, Pommes Wedges und Salat", "Penne mit Thunfischsauce und Salat"),
            new Day("07.04.2022", "Rindsrouladen mit Teigwaren und Salat", "Gebackener Emmentaler mit Sauce Tartare und Salat"),
            new Day("08.04.2022", "Puten Pariser mit Pommes frites und Salat", "Tofu mit Reis und Salat")
        )
    );
}