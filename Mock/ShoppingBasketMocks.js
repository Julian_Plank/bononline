import { Product } from "../Products/Product.js";

export function WarenkorbMocks() {
	this.list = new Array(
		new Product("Kornspitz", "-", "€ 3,00", false),
		new Product("Coca-Cola", "-", "€ 2,50", false),
		new Product("Fanta", "-", "€ 2,50", false),
		new Product("Sprite", "-", "€ 2,50", false),
		new Product("Römerquelle", "-", "€ 1,75", false)
	);
}
