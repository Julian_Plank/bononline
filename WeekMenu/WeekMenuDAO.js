import { BaseDAO } from "../BaseDAO.js";

export function WeekMenuDAO() {
    this.baseDAO = new BaseDAO();
}

WeekMenuDAO.prototype.loadWeekMenu = function(callback, calenderWeek) {
    let xhr = this.baseDAO.prepareRequest("GET", `/weekMenu/${calenderWeek}`, {});
    xhr.onload = () => callback(xhr.response);
    xhr.send();
}