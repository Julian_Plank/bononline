import { createElementFromHtml } from "../Helper/DomUtils.js";
import template from "../Templates/WeekMenu.template.js";
import { ge, getCurrentTime, getDayDifference, getMinuteDifference } from "../Helper/functions.js";
import { ViewHandlerHeader } from "../Helper/ViewHandlerHeader.js";
import { Menu } from "./Menu.js";
import { Modal } from "../Helper/Modal.js";
import { ViewHandlerFooter } from "../Helper/ViewHandlerFooter.js";

export function ViewHandlerWeekMenu() {
    this.header = ge("header");
    this.footer = new ViewHandlerFooter();
    this.vhHeader = new ViewHandlerHeader();
    this.menu;
    this.modal = new Modal();
    this.modal2 = new Modal();
}

ViewHandlerWeekMenu.prototype.renderMenu = function(menu) {
    this.menu = createElementFromHtml(template(menu));
    document.querySelector(".weekmenu").append(this.menu);
};

ViewHandlerWeekMenu.prototype.bindAdd = function(callback, check) {
    let menus = Array.from(document.querySelectorAll(".menu"));
    for (let menu of menus) {
        let menu2 = menu;
        menu.querySelector("#addBtn").onclick = () => {
            let category = menu2.querySelector("#category").value;
            let menu3 = new Menu(
                Array.from(menu2.querySelectorAll(".menuName"))[category - 1].innerHTML,
                Array.from(document.querySelectorAll(".price"))[category - 1].innerHTML,
                menu2.querySelector(".date").innerHTML,
                category,
                1
            );
            callback(menu3);
        };

        if (!this.checkDate(menu)) {
            console.log(menu.querySelector("#addBtn"));
            menu.querySelector("#addBtn").setAttribute("disabled", "");
        }
    }
};

ViewHandlerWeekMenu.prototype.checkDate = function(menu) {
    let date = new Date();
    let todayArray = (date.getDate() + "." + (date.getMonth() + 1) + "." + date.getHours() + ":" + date.getMinutes()).split(".");
    let menuDateArray = menu.querySelector(".date").innerHTML.split(".");
    console.log(menuDateArray, todayArray);
    if (Number(todayArray[0]) > menuDateArray[0]) {
        return false;
    } else if (Number(todayArray[0]) < menuDateArray[0]) {
        return true;
    } else {
        if (Number(todayArray[2].split(":")[0]) < 13) {
            return true;
        } else {
            return false;
        }
    }
};

ViewHandlerWeekMenu.prototype.showModalForSuccessfullyAddedMenu = function() {
    this.modal.modifyBody("Menü wurde zum Warenkorb hinzugefügt! Zum Warenkorb gehen?");
    this.modal.container.querySelector("#yes").onclick = function() {
        window.location.href = "/ShoppingBasket/ShoppingBasket.html";
    };
    jQuery(this.modal.container).modal("show");
};

ViewHandlerWeekMenu.prototype.showMenuNotOnDifferentDays = function() {
    let tmpBtn = document.createElement("button");
    tmpBtn.type = "button";
    tmpBtn.id = "ok";
    tmpBtn.className = "btn btn-success";
    tmpBtn.innerHTML = "Ok";

    this.modal2.modifyBody("Sie können nicht mehrer Menüs an verschiedenen Tagen bestellen!");

    this.modal2.container.querySelector(".modal-footer").innerHTML = "";
    this.modal2.container.querySelector(".modal-footer").append(tmpBtn);
    this.modal2.container.querySelector("#ok").onclick = () => {
        jQuery(this.modal2.container).modal("hide");
    };
    jQuery(this.modal2.container).modal("show");
};