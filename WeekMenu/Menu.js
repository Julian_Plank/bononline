export function Menu(name, price, date, category, amount) {
    this.name = name;
    this.price = price;
    this.date = date;
    this.category = category;
    this.amount = amount;
    this.type = "menu";
}