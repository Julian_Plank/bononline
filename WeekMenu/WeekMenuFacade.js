import { ViewHandlerWeekMenu } from "./ViewHandlerWeekMenu.js";
import { WeekMenuDAO } from "./WeekMenuDAO.js";
import { getWeek, validateAuth, readCookie, checkIfMenuAlreadyExists, modifyAmountMenu } from "../Helper/functions.js";
import { UserDAO } from "../UserDAO.js"

export function WeekMenuFacade() {
    validateAuth();
    this.vH = new ViewHandlerWeekMenu();
    this.weekMenuDAO = new WeekMenuDAO();
    this.menus;
    this.menu;
    this.userDAO = new UserDAO();
    this.user;
}

WeekMenuFacade.prototype.loadUser = function() {
    this.userDAO.loadUser((data) => {
        this.user = JSON.parse(data);
        this.addMenuToShoppingBasket();
    }, readCookie("JSESSIONID"));
}

WeekMenuFacade.prototype.loadWeekMenu = function() {
    this.weekMenuDAO.loadWeekMenu((menus) => {
        this.menus = JSON.parse(menus);
        this.updateView();
        this.loadUser();
    }, getWeek(new Date()));
};

WeekMenuFacade.prototype.checkMenuDate = function(menu) {
    let check = false;
    if (this.user.shoppingbasket.length != 0) {
        for (let p of this.user.shoppingbasket) {
            console.log(p, menu);
            if (p.type == "menu") {
                if (p.date == menu.date) {
                    console.log(p, menu);
                    check = true;
                } else {
                    return false;
                }
            } else {
                check = true;
            }
        }
        return check;
    } else {
        return true;
    }
}

WeekMenuFacade.prototype.addMenuToShoppingBasket = function() {
    this.vH.bindAdd((menu) => {
        if (!this.checkMenuDate(menu)) {
            this.vH.showMenuNotOnDifferentDays();
        } else {
            this.vH.showModalForSuccessfullyAddedMenu();
            this.menu = menu;
            this.user.shoppingbasket.push(this.menu);
            this.userDAO.updateShoppingBasket((data) => {}, {
                sessionid: document.cookie.split("=")[1],
                basket: this.user.shoppingbasket
            })
        }
    })
}

WeekMenuFacade.prototype.updateView = function() {
    this.vH.renderMenu(this.menus);
};

WeekMenuFacade.prototype.init = function() {
    this.vH.vhHeader.createHeader(false, false, true, false, false);
    this.loadWeekMenu();
};

let f = new WeekMenuFacade();
f.init();