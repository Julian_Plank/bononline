"use strict"

import { ProductDAO } from "./ProductDAO.js"
import { ViewHandlerProduct } from "./ViewHandlerProduct.js"
import { validateAuth } from "../helper/functions.js"

function ProductFacade() {
    validateAuth();
    this.productDao = new ProductDAO();
    this.productList = [];
    this.vH = new ViewHandlerProduct();
}

ProductFacade.prototype.loadProducts = function() {
    this.productDao.loadProducts(list => {
        console.log(list)
        this.productList = JSON.parse(list);
        this.updateView();
        this.vH.bindAnsehen();
    })
}

ProductFacade.prototype.updateView = function() {
    this.vH.renderProducts(this.productList);
}

ProductFacade.prototype.init = function() {
    this.loadProducts();
    this.vH.vhHeader.createHeader(false, true, false, false, false);
}

let f = new ProductFacade();
f.init();