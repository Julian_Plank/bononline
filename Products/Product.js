"use strict";

export function Product(name, description, price, favourite) {
	this.name = name;
	this.description = description;
	this.price = price;
	this.favourite = favourite;
}

function toString() {
	return "Product(Name:" + this.name + ", Description:" + this.description + ", Price:" + this.price + ", Favourite:" + this.favourite + ")";
}
