import { renderProduct } from "../Templates/Product.template.js";
import { ge } from "../Helper/functions.js";
import { ViewHandlerHeader } from "../Helper/ViewHandlerHeader.js";
import { ViewHandlerFooter } from "../Helper/ViewHandlerFooter.js";

export function ViewHandlerProduct() {
	this.productsElement = ge("products");
	this.vhHeader = new ViewHandlerHeader();
	this.footer = new ViewHandlerFooter();
}

ViewHandlerProduct.prototype.bindAnsehen = function () {
	let productArray = Array.from(document.querySelectorAll(".column"));
	for (let i = 0; i != productArray.length; i++) {
		productArray[i].querySelector("#ansehenBtn").onclick = () => {
			localStorage.setItem("descriptionId", JSON.stringify(productArray[i].dataset.id));
			window.location = "/ProductDescription/ProductDescription.html";
		};
	}
};

ViewHandlerProduct.prototype.createDiv = function (className, id, innerHTML = "") {
	let div = document.createElement("div");
	div.className = className;
	div.id = id;
	div.innerHTML = innerHTML;
	return div;
};

ViewHandlerProduct.prototype.renderProducts = function (productList) {
	let products = document.createElement("div");
	products.id = "myProducts";
	let id = 0;
	let index = 0;
	let categoryForCategory = ["Kalte Speise", "Warme Speise", "Getränk", "Energys", "Früchte", "Süßes", "Eis"];
	let categoryForTitle = ["Kalte Speisen", "Warme Speisen", "Getränke", "Energy Drinks", "Obst", "Süßes", "Eis"];
	let categoryForId = ["coldMeal", "warmMeal", "drinks", "energies", "fruits", "sweet", "iceCream"];

	if (productList != undefined) {
		for (let category of categoryForCategory) {
			let label = this.createDiv("labelForCategory", categoryForId[index], categoryForTitle[index]);
			products.append(label);
			let list = this.createDiv("row", categoryForTitle[index]);
			for (let product of productList) {
				if (product.category == category) {
					let productElem = renderProduct(product);
					let tmp = document.createElement("template");
					tmp.innerHTML = `<p><button id="ansehenBtn" class="btn">Ansehen</button></p>`;
					productElem.querySelector("h5").after(tmp.content);
					list.append(productElem);
				}
				id++;
			}
			index++;
			products.append(list);
		}
		this.createDropdownMenu();
		this.createSearch(products);
	}

	this.productsElement.replaceWith(products);
	this.productsElement = this.productsElement.list;
};

ViewHandlerProduct.prototype.createDropdownMenu = function () {
	let div = document.createElement("div");
	div.className = "blockContainer";
	div.innerHTML = `<div class="dropdown">
        <button class="dropbtn">Kategorien</button>
        <div class="dropdown-content" id="dropdown-content">
            <a href="#coldMeal">Kalte Speisen</a>
            <a href="#warmMeal">Warme Speisen</a>
            <a href="#drinks">Getränke</a>
            <a href="#energies">Energy Drinks</a>
            <a href="#fruits">Obst</a>
            <a href="#sweet">Süßes</a>
            <a href="#iceCream">Eis</a>
        </div>
    </div>`;
	this.productsElement.before(div);
};

ViewHandlerProduct.prototype.createSearch = function (list) {
	let div = document.createElement("div");
	div.className = "search";
	let label = document.createElement("label");
	label.className = "labelForSearch";
	label.innerHTML = "Produktsuche:";
	div.append(label);
	let search = document.createElement("input");
	search.type = "text";
	search.placeholder = "Suche...";
	search.className = " col-md-2";
	search.id = "searchBar";
	search.onkeyup = () => {
		this.searchProduct(search.value, list);
	};
	div.appendChild(search);
	document.querySelector(".dropdown").after(div);
};

ViewHandlerProduct.prototype.searchProduct = function (filter, list) {
	filter = filter.toLowerCase();
	let cardContainer = ge("myProducts");
	let cards = cardContainer.getElementsByClassName("card");
	let divColumns = cardContainer.querySelectorAll(".column");
	let labels = Array.from(cardContainer.querySelectorAll(".labelForCategory"));
	for (let l of labels) {
		l.style.display = "none";
	}
	if (filter == "") {
		for (let l of labels) {
			l.style.display = "";
		}
	}
	for (let i = 0; i < cards.length; i++) {
		let title = cards[i].querySelector(".card-title");
		if (title.innerText.toLowerCase().indexOf(filter) > -1) {
			divColumns[i].style.display = "";
		} else {
			divColumns[i].style.display = "none";
		}
	}
};
