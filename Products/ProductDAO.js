import { BaseDAO } from "../BaseDAO.js";
export function ProductDAO() {
    this.baseDAO = new BaseDAO();
}

ProductDAO.prototype.loadProducts = function(callback) {
    let xhr = this.baseDAO.prepareRequest("GET", "/products", {});
    xhr.onload = () => callback(xhr.response);
    xhr.send();
}

ProductDAO.prototype.loadProduct = function(callback, id) {
    let xhr = this.baseDAO.prepareRequest("GET", `/products/${id}`, {});
    console.log(xhr);
    xhr.onload = () => callback(xhr.response);
    xhr.send();
}

ProductDAO.prototype.createProducts = function(callback, product) {
    let xhr = this.baseDAO.prepareRequest("POST", "/products", {});
    xhr.onload = () => callback(xhr.response);
    xhr.send(JSON.stringify(product));
}